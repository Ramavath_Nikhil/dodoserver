 <?php

 session_start();

 if (!empty($_SESSION["user_details"])) {

   //  echo "<script>window.location.href = 'views/booking_step_1.php';</script>";
 }
 ?>


 <!DOCTYPE html>
 <html lang="en">
 <head>
  <title>Welcome | DODO</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://www.w3schools.com/lib/w3.css">
  <link href="assets/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
  <link href="assets/css/styles.css" rel="stylesheet" type="text/css"/>
  <script src="assets/js/jquery-2.2.0.js" type="text/javascript"></script>
  <script src="assets/js/bootstrap.js" type="text/javascript"></script>
  <script src="assets/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
  <script src="assets/js/main.js" type="text/javascript"></script>
  <script src="assets/js/jquery.slides.min.js"></script>
<script>
$(document).on('mouseover','.check_up',function(){
    $('.checkUp').fadeIn();
});
$(document).on('mouseleave','.check_up',function(){
    $('.checkUp').fadeOut();
});
$(document).on('mouseover','.consulTation',function(){
    $('.consuLta').fadeIn();
});
$(document).on('mouseleave','.consulTation',function(){
    $('.consuLta').fadeOut();
});
</script>
</head>
<body>

    <?php

      if (!empty($_SESSION["user_details"])) {

echo "<input type='hidden' id='hidden_id' value='".$_SESSION["user_id"]."'>";

}
       ?>



  <nav class="navbar navbar-inverse">
    <div class="container">
      <div class="navbar-header">
        <a class="navbar-brand" href="#">
          <img src="assets/images/logo.png" alt=""/>
        </a>
      </div>


      <?php

      if (empty($_SESSION["user_details"])) {

       ?>


       <div class="right_sec">
        <div data-toggle="modal" data-target="#loginForm" class="btn_prim">Login</div>
      </div>


      <?php
    }


    ?>








  </div>
</nav>
<div class="container-fluid">
  <div class="image_sec">
    <img src="assets/images/doc_ind.jpg" alt=""/>
    <div class="blue_mask"></div>
    <div class="text_sec">
      <h2>See A Doctor Now - In The Comfort Of Your Apartment</h2>
      <h4><span class="underline check_up">Health Checkup</span> + Specialist Doctor <span class="underline consulTation">Consultation</span></h4>
      <div class="checkUp">
          
      </div>
      <div class="consuLta">
          
      </div>
      <?php

      if (empty($_SESSION["user_details"])) {

       ?>



       <div class="btn_prim btn_reg" data-toggle="modal" data-target="#registerForm">REGISTER</div>

       <?php
     }

     else
     {
      ?>

      <div class="btn_prim btn_reg" data-toggle="modal" id="book_appointment_appartment" data-target="#bookAppointmentForm">BOOK APPOINTMENT</div>
      <?php

    }
    ?>


  </div>
</div>
<div class="dodo_info">
  <h3>What is DoDo?</h3>
  <div class="info_dod">
    <div class="one_info">
      <img src="assets/images/notepad.png" alt=""/>
      <h5>HIGH QUALITY CARE</h5>
      <p>We hand pick specialist after a vetting process</p>
    </div> 
    <div class="one_info">
      <img src="assets/images/calendar.png" alt=""/>
      <h5>Calendar</h5>
      <p>Check calender for doctor schedules, and book appointments easily on the DoDo website </p>
      <p>The opportunity of having a 1:1 with specialist on a regular basis has never been so easy</p>
    </div> 
    <div class="one_info">
      <img src="assets/images/roof.png" alt=""/>
      <h5>CONVENIENT</h5>
      <p>No more to travelling and waiting in clinics and hospitals.</p>
      <p>Personalized and quality care is at your door steps.</p>
    </div> 
    <div class="one_info">
      <img src="assets/images/cardiogram.png" alt=""/>
      <h5>PREVENTIVE CHECK UPS</h5>
      <p>Two health check ups in a year.</p>
      <p>Health checks help identify problems early on.</p>
    </div> 
    <div class="one_info">
      <img src="assets/images/budget.png" alt=""/>
      <h5>AFFORDABLE</h5>
      <p>40% lower cost compared to corporate pricing.</p>
      <p>Also save on travel and waiting time.</p>
    </div> 
  </div>
</div>
<div class="about_sec">
  <h2>ABOUT US</h2>
  <h5>We simply how patients access care!</h5>
  <p>Our mission is to transform healthcare by offering patients better tools and better access to high quality care at home. Our website gives you access to the best care from the Doctors near your location at your apartment. </p>
</div>
</div>
<!-- Modal -->
<div id="loginForm" class="modal fade" role="dialog">
  <div class="modal-dialog login_modal">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Login</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <!--<label for="uname">Username:</label>-->
                <input id = "login_email" type="text" class="form-control" id="uname" placeholder="Username">
              </div>
              <div class="form-group">
                <!--<label for="pwd">Password:</label>-->
                <input id="login_password" type="password" class="form-control" id="pwd" placeholder="Password">
              </div>
              <div class="pull-right">
                <div class="checkbox">
                </div>
                <button id="login_button"  type="button" class="btn btn-default">Login</button>
                <img class="log_img" src="assets/images/loader.gif" alt="" style="display: none"/>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!--Register form-->
<div id="registerForm" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Register</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">
            <div class="col-sm-12">
              <div class="col-sm-6">
                <div class="form-group">
                  <!--<label for="fname">First name:</label>-->
                  <input type="text" class="form-control" id="fName" placeholder="First name">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <!--<label for="lname">Last name:</label>-->
                  <input type="text" class="form-control" id="lname" placeholder="Last name">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <!--<label for="email">Email:</label>-->
                  <input type="email" class="form-control" id="regMail" placeholder="Email">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <!--<label for="phone">Phone number:</label>-->
                  <input type="email" class="form-control" id="phone" placeholder="Phone number">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <!--<label for="pwd">Password: </label>-->
                  <input type="password" class="form-control" id="rPwd" placeholder="Password">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <!--<label for="cpass">Confirm password:</label>-->
                  <input type="password" class="form-control" id="cpass" placeholder="Confirm password">
                </div>
              </div>
              <div style="display:none;" class="col-sm-6">
                <div class="form-group">
                  <!--<label for="pfLanguage">Preferred first language:</label>-->
                  <select class="form-control" id="pfLanguage">
                    <option value="">Preferred first language</option>
                    <option value="Hindi">Hindi</option>
                    <option value="English">English</option>
                    <option value="Telugu">Telugu</option>
                    <option value="Urdu">Urdu</option>
                  </select>
                </div>
              </div>
              <div style="display:none;" class="col-sm-6">
                <div class="form-group">
                  <!--<label for="psLanguage">Preferred second language: </label>-->
                  <select class="form-control" id="psLanguage">
                    <option value="">Preferred second language</option>
                    <option value="Hindi">Hindi</option>
                    <option value="English">English</option>
                    <option value="Telugu">Telugu</option>
                    <option value="Urdu">Urdu</option>
                  </select>
                </div>
              </div>
              <div style="display:none;" class="col-sm-6">
                <div class="form-group">
                  <!--<label for="ptLanguage">Preferred third language:</label>-->
                  <select class="form-control" id="ptLanguage">
                    <option value="">Preferred third language</option>
                    <option value="Hindi">Hindi</option>
                    <option value="English">English</option>
                    <option value="Telugu">Telugu</option>
                    <option value="Urdu">Urdu</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="pull-right">
                  <!--<label for="register">&nbsp;</label>-->
                  <button type="button" class="btn btn-default" id="register" style="margin-top: 5px">Register</button>
                  <img class="reg_img" src="assets/images/loader.gif" alt="" style="display: none"/>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  </div>
  <div id="bookAppointmentForm" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirm your Appointment</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">
              <div class="col-sm-12">
              <h4>Are you sure you want to book your appointment?</h4>
                <div class="pull-right">
                  <!--<label for="register">&nbsp;</label>-->
                  <button type="button" class="btn btn-default btn-danger" data-dismiss="modal" style="margin-top: 5px">Cancel</button>
                  <button type="button" class="btn btn-default" id="confirm_appointment" style="margin-top: 5px">Confirm</button>
                  <img class="reg_img" src="assets/images/loader.gif" alt="" style="display: none"/>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
