 <?php
    session_start();
    if (empty($_SESSION["user_details"])) {

         echo "<script>window.location.href = '../index.php';</script>";
    }
?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Booking Process</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/lib/w3.css">
        <link href="../assets/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <script src="../assets/js/jquery-2.2.0.js" type="text/javascript"></script>
        <script src="../assets/js/bootstrap.js" type="text/javascript"></script>
        <script src="../assets/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../assets/js/main.js" type="text/javascript"></script>

    </head>
    <body>
       <nav class="navbar navbar-inverse">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>                        
                </button>
                  <a class="navbar-brand" href="#">
                      <img src="../assets/images/logo.png" alt=""/>
                  </a>
              </div>
              <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="#">About</a></li>
                  <li><a href="#">Logout</a></li>
                  <li><a data-toggle="modal" data-target="#changeProfile">Profile</a></li>
                </ul>
              </div>
            </div>
        </nav>
        <div class="container">
            <h2>Select your booking date:</h2>
            <div class="row">
                <div class="col-sm-4 dummy_cls">
                    <div class="w3-panel w3-card-4 temp_width">
                        <h3>Select Date</h3>
                        <div name="datepicker1" class="datePic"></div>
                        <button class="btn btn-primary search_btn">Search Doctors</button>
                    </div>
                </div>
                <div class="loader" style="display: none">
                    <img src="../assets/images/loader.gif" alt=""/>
                </div>
                <div class="col-sm-8 doctors_list" style="display: none">
                    <div id="print_doctor_data" class="w3-panel w3-card-4">
                         
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade registerForm" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 id = "doctor_name" class="modal-title">Doctor name</h4>
              </div>
              <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-sm-12">
                            <h5  id="doa" class="high_txt">Date of Appointment: 18/4/2017</h5>
                            <div class="form-group">
                                <label>Available Time Slots:</label>
                                <div class="time_pic" id="print_available_slots">
                                    
                                </div>
                            </div>
                            <div class="pull-right">
                                <button type="reset" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="button"  id = "book_appointment" class="btn btn-primary">Confirm</button>
                            </div>
                        </div>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div id="changeProfile" class="modal fade" role="dialog">
            <div class="modal-dialog login_modal">
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Profile Settings</h4>
                  </div>
                  <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="pof_pic">
                                        <div id="mybutton">
                                            <img src="../assets/images/profile_teaser.png" alt=""/>
                                            <input type="file" id="myfile" name="upload"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                      <!--<label for="fname">First name:</label>-->
                                      <input type="text" class="form-control" id="fName" placeholder="First name">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                      <!--<label for="lname">Last name:</label>-->
                                      <input type="text" class="form-control" id="lname" placeholder="Last name">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                      <!--<label for="email">Email:</label>-->
                                      <input type="email" class="form-control" id="regMail" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                      <!--<label for="phone">Phone number:</label>-->
                                      <input type="email" class="form-control" id="phone" placeholder="Phone number">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                      <!--<label for="pwd">Password: </label>-->
                                      <input type="password" class="form-control" id="rPwd" placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                      <!--<label for="cpass">Confirm password:</label>-->
                                      <input type="password" class="form-control" id="cpass" placeholder="Confirm password">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                      <!--<label for="pfLanguage">Preferred first language:</label>-->
                                      <select class="form-control" id="pfLanguage">
                                          <option value="">Preferred first language</option>
                                          <option value="Hindi">Hindi</option>
                                          <option value="English">English</option>
                                          <option value="Telugu">Telugu</option>
                                          <option value="Urdu">Urdu</option>
                                      </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                      <!--<label for="psLanguage">Preferred second language: </label>-->
                                      <select class="form-control" id="psLanguage">
                                          <option value="">Preferred second language</option>
                                          <option value="Hindi">Hindi</option>
                                          <option value="English">English</option>
                                          <option value="Telugu">Telugu</option>
                                          <option value="Urdu">Urdu</option>
                                      </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                      <!--<label for="ptLanguage">Preferred third language:</label>-->
                                      <select class="form-control" id="ptLanguage">
                                          <option value="">Preferred third language</option>
                                          <option value="Hindi">Hindi</option>
                                          <option value="English">English</option>
                                          <option value="Telugu">Telugu</option>
                                          <option value="Urdu">Urdu</option>
                                      </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="pull-right">
                                        <!--<label for="register">&nbsp;</label>-->
                                        <button type="button" class="btn btn-default" id="register" style="margin-top: 5px">Update</button>
                                        <img class="reg_img" src="assets/images/loader.gif" alt="" style="display: none"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                  </div>
                </div>
            </div>
        </div>
    </body>
</html>
