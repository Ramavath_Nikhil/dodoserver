<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Nikhil Ramavath
 */
class DbHandler {

    private $conn;

    function __construct() {
        require_once  'DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    /* ------------- `users` table method ------------------ */

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function createUser($first_name,$last_name,$email,$mobile,$password,$first_language,$second_language,$third_language) {
        require_once 'PassHash.php';
        $response = array();

        // First check if user already existed in db
        if (!$this->isUserExists($email)) {
            // Generating password hash
            $password_hash = PassHash::hash($password);

            // Generating API key
            $api_key = $this->generateApiKey();

            // insert query
            $stmt = $this->conn->prepare("INSERT INTO users(first_name,last_name, email,mobile, password, api_key, status,first_language,second_language,third_language) values(?, ?, ?,?,?,?, 1,?,?,?)");
            $stmt->bind_param("sssssssss", $first_name,$last_name, $email, $mobile,$password_hash, $api_key,$first_language,$second_language,$third_language);

            $result = $stmt->execute();

            $stmt->close();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return USER_CREATE_FAILED;
            }
        } else {
            // User with same email already existed in the db
            return USER_ALREADY_EXISTED;
        }

        return $response;
    }


/**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
public function createDoctor($name, $age,$email,$mobile, $password,$gender,$location,$latlong,$specialiation,$experience,$additional_degree,$time_slots) {
    require_once 'PassHash.php';
    $response = array();

        // First check if user already existed in db
    if (!$this->isDoctorExists($email)) {
            // Generating password hash
        $password_hash = PassHash::hash($password);

            // Generating API key
        $api_key = $this->generateApiKey();

        $date = date('Y/m/d H:i:s', time());


            // insert query
        $stmt = $this->conn->prepare("INSERT INTO `doctors`(`name`, `email`, `mobile`, `age`, `password`,`gender`, `location`, `latlong`, `specialization`, `experience`, `additional_degree`, `timeslots`, `created_at`, `status`, `api_key`) values(?, ?, ?,?,?,?,?,?,?,?,?,?, ?,1,?)");
        $stmt->bind_param("ssssssssssssss", $name, $email, $mobile,$age,$password_hash, $gender,$location,$latlong,$specialiation,$experience,$additional_degree,$time_slots,$date,$api_key);

        $result = $stmt->execute();

        $stmt->close();

            // Check for successful insertion
        if ($result) {
                // User successfully inserted
            return USER_CREATED_SUCCESSFULLY;
        } else {
           
                // Failed to create user
            return USER_CREATE_FAILED;
        }
    } else {
            // User with same email already existed in the db
        return USER_ALREADY_EXISTED;
    }

    return $response;
}


  /**
     * Updating profile picpath basing on email
     * @param String $profilepicpath path of the image
     * @param String $email email of the user
     *  
     */
  public function updateProfilePicPath($profilepicpath, $email) {
    $stmt = $this->conn->prepare("UPDATE users set profile_pic = ? where email = ?");
    $stmt->bind_param("ss", $profilepicpath, $email);
    $stmt->execute();
    $num_affected_rows = $stmt->affected_rows;
    $stmt->close();
    return $num_affected_rows > 0;
}


/**
     * Updating profile picpath basing on email
     * @param String $profilepicpath path of the image
     * @param String $email email of the user
     *  
     */
public function updateProfilePicPathOfDoctor($profilepicpath, $email) {
    $stmt = $this->conn->prepare("UPDATE doctors set profile_pic = ? where email= ?");
    $stmt->bind_param("ss", $profilepicpath, $email);
    $stmt->execute();
    $num_affected_rows = $stmt->affected_rows;
    $stmt->close();
    return $num_affected_rows > 0;
}





  /**
     * Updating profile picpath of the care taker basing on mobile
     * @param String $profilepicpath path of the image
     * @param String $email email of the user
     *  
     */
  public function updateProfilePicPathOfCaretaker($profilepicpath, $id) {
    $stmt = $this->conn->prepare("UPDATE caretakers set profile_pic = ? where id = ?");
    $stmt->bind_param("ss", $profilepicpath, $id);
    $stmt->execute();
    $num_affected_rows = $stmt->affected_rows;
    $stmt->close();
    return $num_affected_rows > 0;
}


 /**
     * Updating profile picpath of the care taker basing on mobile
     * @param String $profilepicpath path of the image
     * @param String $email email of the user
     *  
     */
  public function updatePoliceCertifcateOfCaretaker($certificatepath, $id) {
    $stmt = $this->conn->prepare("UPDATE caretakers set police_certificate = ? where id = ?");
    $stmt->bind_param("ss", $certificatepath, $id);
    $stmt->execute();
    $num_affected_rows = $stmt->affected_rows;
    $stmt->close();
    return $num_affected_rows > 0;
}

 /**
     * Updating profile picpath of the care taker basing on mobile
     * @param String $profilepicpath path of the image
     * @param String $email email of the user
     *  
     */
  public function updateExtracertificate($extraCertificatePath, $id) {
     $stmt = $this->conn->prepare("UPDATE caretakers set extra_certificate = ? where id = ?");

        $stmt->bind_param("ss", $extraCertificatePath, $id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
}


/**
     * Updating profile picpath of the care taker basing on mobile
     * @param String $profilepicpath path of the image
     * @param String $email email of the user
     *  
     */
  public function updateAadharCard($aadharCardPath, $id) {
    $stmt = $this->conn->prepare("UPDATE caretakers set `ID/Address_proof` = ? where id = ?");
    $stmt->bind_param("ss", $aadharCardPath, $id);
    $stmt->execute();
    $num_affected_rows = $stmt->affected_rows;
    $stmt->close();
    return $num_affected_rows > 0;
}



      /**
     * Updating user's last project location
     * @param String $latlong latlong of the location
     * @param String $email email of the user
     *  
     */
      public function updateUserLastProjectLocation($last_project_location, $id) {
        $stmt = $this->conn->prepare("UPDATE users set last_project_location = ? where id = ?");
        $stmt->bind_param("ss", $last_project_location, $id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

     /**
     * Updating profile picpath basing on email
     * @param String $profilepicpath path of the image
     * @param String $email email of the user
     *  
     */
     public function updateProfilePicPathByUserId($profilepicpath, $user_id) {
        $stmt = $this->conn->prepare("UPDATE users set profile_pic = ? where id = ?");
        $stmt->bind_param("ss", $profilepicpath, $user_id['id']);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }


     /**
     * Updating doctor profile picpath basing on id
     * @param String $profilepicpath path of the image
     * @param String $user_id id of the user
     *  
     */
     public function updateDoctorProfilePicPathByUserId($profilepicpath, $user_id) {
        $stmt = $this->conn->prepare("UPDATE doctors set profile_pic = ? where id = ?");
        $stmt->bind_param("ss", $profilepicpath, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

     /**
     * Updating profile picpath basing on id
     * @param String $profilepicpath path of the image
     * @param String $id id of the user
     *  
     */
     public function updateCareTakerProfilePicture($profilepicpath, $id) {
        $stmt = $this->conn->prepare("UPDATE caretakers set profile_pic = ? where id = ?");

        $stmt->bind_param("ss", $profilepicpath, $id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }


 /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
 public function createCareTaker($organisation_id, $name,$email,$mobile, $gender,$age,$doj,$qualification,$experience,$designation,$addressproof,$blood_group,$martial_status,$residence_number,$father,$mother,$spouse,$children,$background_verification,$address,$height,$weight,$languages) {
     
    $response = array();

        // First check if user already existed in db
   // if (!$this->isCareTakerExists($mobile)) {
       
            // insert query
        $stmt = $this->conn->prepare("INSERT INTO `caretakers`(`organisation_id`, `caretaker_name`, `email`, `mobile`, `gender`, `address`, `age`, `DOJ`, `qualification`, `experience`, `designation`, `residence_number`, `ID/Address_proof`, `blood_group`, `father`, `mother`, `martial_status`, `spouse`, `children`, `background_verification`, `status`,`height`,`weight`,`languages`)  values(?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,0,?,?,?)");

        $stmt->bind_param("sssssssssssssssssssssss", $organisation_id,$name, $email, $mobile,$gender,$address,$age,$doj,$qualification,$experience,$designation,$residence_number,$addressproof,$blood_group,$father,$mother,$martial_status,$spouse,$children,$background_verification,$height,$weight,$languages);
        
        $result = $stmt->execute();

        $stmt->close();

            // Check for successful insertion
        $response = array();
        if ($result) {
                // User successfully inserted
            $new_task_id = $this->conn->insert_id;
            $response["result"] = USER_CREATED_SUCCESSFULLY;
            $response["id"] = $new_task_id;
            
            return $response;
        } else {
                // Failed to create user
            $response["result"] = USER_CREATE_FAILED;
            return $response;
        }
   /* } else {
            // User with same mobile already existed in the db
        return USER_ALREADY_EXISTED;
    }*/

    return $response;
}



 /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
 public function createOrganisation($name,$concerned_person,$email,$password,$mobile,$message,$type) {
     
   require_once 'PassHash.php';
   $response = array();

        // First check if user already existed in db
   if (!$this->isOrganisationExists($email)) {
       
     $password_hash = PassHash::hash($password);

            // insert query
     $stmt = $this->conn->prepare("INSERT INTO `organisations` (`name`, `concerened_person`, `email`, `password`, `mobile`, `message`, `status`,`type`) values(?, ?, ?,?,?,?, 1,?)");

     $stmt->bind_param("sssssss", $name,$concerned_person, $email, $password_hash,$mobile,$message,$type);
     
     $result = $stmt->execute();

     $stmt->close();

            // Check for successful insertion
     if ($result) {
                // User successfully inserted
        return USER_CREATED_SUCCESSFULLY;
    } else {
                // Failed to create user
        return USER_CREATE_FAILED;
    }
} else {
            // User with same email already existed in the db
    return USER_ALREADY_EXISTED;
}

return $response;
}


    /**
     * Checking user login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($email, $password) {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT password FROM users WHERE email = TRIM(?)");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->bind_result($password_hash);

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // Found user with the email
            // Now verify the password

            $stmt->fetch();

            $stmt->close();

            if (PassHash::check_password($password_hash, $password)) {
                // User password is correct
                return TRUE;
            } else {
                // user password is incorrect
                return FALSE;
            }
        } else {
            $stmt->close();

            // user not existed with the email
            return FALSE;
        }
    }


      /**
     * Checking organisation login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
      public function checkOrganisationLogin($email, $password) {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT password FROM organisations WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->bind_result($password_hash);

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // Found user with the email
            // Now verify the password

            $stmt->fetch();

            $stmt->close();

            if (PassHash::check_password($password_hash, $password)) {
                // User password is correct
                return TRUE;
            } else {
                // user password is incorrect
                return FALSE;
            }
        } else {
            $stmt->close();

            // user not existed with the email
            return FALSE;
        }
    }


     /**
     * Checking organisation login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
      public function checkDoctorLogin($email, $password) {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT password FROM doctors WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->bind_result($password_hash);

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // Found user with the email
            // Now verify the password

            $stmt->fetch();

            $stmt->close();

            if (PassHash::check_password($password_hash, $password)) {
                // User password is correct
                return TRUE;
            } else {
                // user password is incorrect
                return FALSE;
            }
        } else {
            $stmt->close();

            // user not existed with the email
            return FALSE;
        }
    }



    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($email) {
        $stmt = $this->conn->prepare("SELECT id from users WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }


 /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
 private function isDoctorExists($email) {
    $stmt = $this->conn->prepare("SELECT id from doctors WHERE email = ?");
    $stmt->bind_param("s", $email);
    $stmt->execute();
    $stmt->store_result();
    $num_rows = $stmt->num_rows;
    $stmt->close();
    return $num_rows > 0;
}



     /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
     private function isCareTakerExists($mobile) {
        $stmt = $this->conn->prepare("SELECT id from caretakers WHERE mobile = ?");

        $stmt->bind_param("s", $mobile);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

 /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
 private function isOrganisationExists($email) {
    $stmt = $this->conn->prepare("SELECT id from organisations WHERE email = ?");
    $stmt->bind_param("s", $email);
    $stmt->execute();
    $stmt->store_result();
    $num_rows = $stmt->num_rows;
    $stmt->close();
    return $num_rows > 0;
}




    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function getUserByEmail($email) {
       
        $stmt = $this->conn->prepare("SELECT first_name,last_name, email, status,api_key as apiKey, status, created_at,mobile,profile_pic,last_project_location as last_project_location,first_language,second_language,third_language,(SELECT GROUP_CONCAT(services.service SEPARATOR '-') FROM services WHERE services.status = '0') as services FROM users WHERE email = ?");
        $stmt->bind_param("s", $email);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }


     /**
     * Fetching doctor details by id
     * @param String $email User email id
     */
    public function fetchDoctorDetailsbyID($id) {
       
        $stmt = $this->conn->prepare("SELECT id,name, email,mobile,profile_pic,age,gender,location,latlong,specialization,experience,additional_degree,  timeslots,created_at,
            status,
            profile_pic,
            fcm_id,
            api_key FROM doctors WHERE id = ?");
        $stmt->bind_param("s", $id);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }



    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function fetchCouponDetails($user_id,$coupon) {
       
        $stmt = $this->conn->prepare("SELECT * FROM `coupons` WHERE coupons.code = ? and coupons.coupon_limit >(SELECT COUNT(user_coupons.id) FROM user_coupons WHERE user_coupons.coupon_id = coupons.id and user_coupons.user_id = ?)");
        $stmt->bind_param("ss", $coupon,$user_id['id']);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }




     /**
     * Fetching user by email
     * @param String $email User email id
     */
     public function getQuotedProjectsByOrganisation($organisation_id) {
       
        $stmt = $this->conn->prepare("SELECT projects.*,caretakers.caretaker_name,projectquotes.id as projectquote_id,projectquotes.coated_price  as projectquotes_coatedprice,projectquotes.created_at as project_coated_on,projectquotes.caretaker_id as project_caretaker_id FROM `projectquotes` INNER JOIN projects INNER JOIN caretakers WHERE projectquotes.`caretaker_id` IN (select caretakers.id from caretakers where caretakers.organisation_id = ?) and projects.id = projectquotes.project_id and projects.status = 0 and caretakers.id = projectquotes.caretaker_id ");
        $stmt->bind_param("s", $organisation_id);
        if ($stmt->execute()) {
            $user = $stmt->get_result();
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }



     /**
     * Fetching user by email
     * @param String $email User email id
     */
     public function getDoctorsSpecilizations() {
       
        $stmt = $this->conn->prepare("SELECT GROUP_CONCAT(DISTINCT doctor_specilizations.specilization SEPARATOR \"-\") AS specilization FROM `doctor_specilizations` WHERE status = '0'");
        
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }




    /**
     * Fetching usercartcount by user_id
     * @param String $user_id User  id
     */
    public function getUserCartCount($user_id) {
       
        $stmt = $this->conn->prepare("SELECT SUM( count ) AS cart_count
            FROM  `cart` 
            WHERE user_id =?");
        $stmt->bind_param("s", $user_id['id']);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

 /**
     * Fetching usercart by user_id
     * @param String $email User email id
     */
 public function fetchOrganisationNameandFCMID($project_id,$caretaker_id) {
   
    $stmt = $this->conn->prepare("SELECT (SELECT organisations.name from organisations WHERE organisations.id = (SELECT caretakers.organisation_id FROM caretakers WHERE caretakers.id = ?)) as organisations_name, (SELECT users.fcm_id FROM users WHERE users.id = (SELECT projects.user_id FROM projects WHERE projects.id = ?)) as fcm_id");
    $stmt->bind_param("ss", $caretaker_id,$project_id);
    if ($stmt->execute()) {
        $user = $stmt->get_result()->fetch_assoc();
        $stmt->close();
        return $user;
    } else {
        return NULL;
    }
}



/**
     * Fetching user name and organisation fcm_id to sending push notification
     * @param String $project_id id of the project
     */
public function fetchUserNameandFCMID($project_id) {
   
    $stmt = $this->conn->prepare("SELECT (select users.first_name from users where users.id = projects.user_id) as first_name , (Select organisations.fcm_id from organisations where organisations.id = (select caretakers.organisation_id from caretakers where caretakers.id = projects.caretaker_id)) as fcm_id  FROM `projects` WHERE projects.id = ?");
    $stmt->bind_param("s", $project_id);
    if ($stmt->execute()) {
        $user = $stmt->get_result()->fetch_assoc();
        $stmt->close();
        return $user;
    } else {
        return NULL;
    }
}



 /**
     * Fetching user by email
     * @param String $email User email id
     */
 public function getOrganisationDetailsbyEmail($email) {
   
    $stmt = $this->conn->prepare("SELECT * FROM organisations WHERE email = ?");
    $stmt->bind_param("s", $email);
    if ($stmt->execute()) {
        $user = $stmt->get_result()->fetch_assoc();
        $stmt->close();
        return $user;
    } else {
        return NULL;
    }
}


/**
     * Fetching doctor by email
     * @param String $email User email id
     */
 public function getDoctorByEmail($email) {
   
    $stmt = $this->conn->prepare("SELECT * FROM doctors WHERE email = ?");
    $stmt->bind_param("s", $email);
    if ($stmt->execute()) {
        $user = $stmt->get_result()->fetch_assoc();
        $stmt->close();
        return $user;
    } else {
        return NULL;
    }
}


    /**
     * Fetching user api key
     * @param String $user_id user id primary key in user table
     */
    public function getApiKeyById($user_id) {
        $stmt = $this->conn->prepare("SELECT api_key FROM users WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            $api_key = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $api_key;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user id by api key
     * @param String $api_key user api key
     */
    public function getUserId($api_key) {
        $stmt = $this->conn->prepare("SELECT id FROM users WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        if ($stmt->execute()) {
            $user_id = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user_id;
        } else {
            return NULL;
        }
    }

    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function isValidApiKey($api_key) {
        $stmt = $this->conn->prepare("SELECT id from users WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateApiKey() {
        return md5(uniqid(rand(), true));
    }

    /* ------------- `tasks` table method ------------------ */




 /**
     * Creating new task
     * @param String $user_id user id to whom task belongs to
     * @param String $task task text
     */
 public function createProject($user_id,$name, $gender,$age,$height,$services,$hours,$duration,$moreinfo,$weight,$latlong,$address) {        
     
       // print_r($user_id);
    $stmt = $this->conn->prepare("INSERT INTO projects(user_id,patient_name,gender,age,height,services,hours,duration,moreinfo,weight,latlong,address) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");
    $stmt->bind_param("ssssssssssss", $user_id['id'],$name,$gender,$age,$height,$services,$hours,$duration,$moreinfo,$weight,$latlong,$address);
    $result = $stmt->execute();
    $stmt->close();

    if ($result) {
            // task row created
            // now assign the task to user
        $new_task_id = $this->conn->insert_id;
            //$res = $this->createUserTask($user_id, $new_task_id);
        if( $this->updateUserLastProjectLocation($latlong,$user_id['id']))
            return $new_task_id;
        else
            return $new_task_id;
        
    } else {
            // task failed to create
        return NULL;
    }
}

    /**
     * Fetching single task
     * @param String $task_id id of the task
     */
    public function getTask($task_id, $user_id) {
        $stmt = $this->conn->prepare("SELECT t.id, t.task, t.status, t.created_at from tasks t, user_tasks ut WHERE t.id = ? AND ut.task_id = t.id AND ut.user_id = ?");
        $stmt->bind_param("ii", $task_id, $user_id);
        if ($stmt->execute()) {
            $task = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $task;
        } else {
            return NULL;
        }
    }


       /**
     * Fetching cart sum
     * @param String $user_id id of the user
     */
       public function getUserCartSum($user_id) {
        $stmt = $this->conn->prepare("SELECT SUM((SELECT products.price from products WHERE products.id = cart.product_id)*cart.count) as product_price FROM `cart` WHERE cart.user_id = ?");
        $stmt->bind_param("i",$user_id['id']);
        if ($stmt->execute()) {
            $task = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $task['product_price'];
        } else {
            return NULL;
        }
    }


    /**
     * Fetching all user tasks
     * @param String $user_id id of the user
     */
    public function getAllUserTasks($user_id) {

        $stmt = $this->conn->prepare("SELECT projects.type as project_type,projects.id as project_id,projects.created_at as posted_date,(SELECT count(*) from projectquotes where projectquotes.project_id = projects.id AND projectquotes.coated_price >0) as count,(SELECT caretakers.caretaker_name from caretakers where caretakers.id = projects.caretaker_id) as caretaker_name FROM `projects` WHERE user_id = ".$user_id['id']." and status IN(0,1)");

         // $stmt->bind_param("i", $user_id['id']);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

/**
     * Fetching all available care takers of an organisation
     * @param String $organisation_id id of the organisation
     */
public function getAvailableCareTakersofOrganisation($organisation_id) {
    $stmt = $this->conn->prepare("SELECT caretakers.caretaker_name,caretakers.id FROM `caretakers` WHERE caretakers.status = 1 and caretakers.organisation_id = ".$organisation_id);

         // $stmt->bind_param("i", $user_id['id']);
    $stmt->execute();
    $tasks = $stmt->get_result();
    $stmt->close();
    return $tasks;
}



/**
     * Fetching all available care takers of an organisation
     * @param String $organisation_id id of the organisation
     */
public function getUserOrders($user_id) {
    $stmt = $this->conn->prepare("SELECT * FROM `orders` WHERE orders.user_id=".$user_id['id']." ORDER BY orders.id DESC");

         // $stmt->bind_param("i", $user_id['id']);
    $stmt->execute();
    $tasks = $stmt->get_result();
    $stmt->close();
    return $tasks;
}


     /**
     * Fetching complete details of a project
     * @param String $project_id id of the project
     */
     public function getCompleteDetailsOfProject($project_id) {
        $stmt = $this->conn->prepare("SELECT projects.*,(SELECT count(*) from projectquotes where projectquotes.project_id = projects.id AND projectquotes.coated_price >0) as count,(SELECT caretakers.caretaker_name from caretakers where caretakers.id = projects.caretaker_id) as caretaker_name,(SELECT organisations.name from organisations WHERE organisations.id = (SELECT caretakers.organisation_id from caretakers where caretakers.id = projects.caretaker_id)) as organisation_name FROM `projects` WHERE  projects.id = ".$project_id);
        
         // $stmt->bind_param("i", $user_id['id']);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }


     /**
     * Fetching all quotes for a project
     * @param String $project_id id of the project
     */
     public function getProjectQuotes($project_id) {
        $stmt = $this->conn->prepare("SELECT *,(SELECT organisations.name from organisations WHERE organisations.id=(SELECT caretakers.organisation_id from caretakers WHERE caretakers.id = projectquotes.caretaker_id)) as organisation_name FROM `projectquotes` WHERE project_id = ".$project_id ." and projectquotes.coated_price>0 ");
        
        
         
       // $stmt->bind_param("i", $user_id['id']);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }


 /**
     * Function to assign a task to user
     * @param String $user_id id of the user
     * @param String $task_id id of the task
     */
    public function createContactus($type, $message,$user_id) {
        
      
        $stmt = $this->conn->prepare("INSERT INTO contactus(fb_id, type,message) values(?, ?,?)");
        $stmt->bind_param("sss", $user_id['id'], $type,$message);
        $result = $stmt->execute();
       

        if (false === $result) {
            die('execute() failed: ' . sqlerror());
        }
        $new_task_id = $this->conn->insert_id;
        $stmt->close();
        return $new_task_id;
    }


    /** Function to update attachment of an contact us query
* @param String $attachment_url url of the attachment
* @param String $task_id id of the task
*/

public Function updateAttachmentUrl($attachment_url,$task_id)
{
      $stmt = $this->conn->prepare("UPDATE contactus set contactus.attachment_url = '".$attachment_url."' where contactus.id = ?");
 
        $stmt->bind_param("s",$task_id);
        $result = $stmt->execute();

        if (false === $result) {
            die('execute() failed: ' . htmlspecialchars($stmt->error));
        }
        $stmt->close();
        return $result; 
}



 /**
     * Fetching all caretakers under an organisation
     * @param String $organisation_id id of the organisation
     */
 public function getCareTakersOfAnOrganisation($organisation_id) {
    $stmt = $this->conn->prepare("SELECT id,organisation_id,caretaker_name,mobile,gender,status,profile_pic,email FROM `caretakers` WHERE caretakers.organisation_id = ".$organisation_id ." and caretakers.status = 1");
    
     
       // $stmt->bind_param("i", $user_id['id']);
    $stmt->execute();
    $tasks = $stmt->get_result();
    $stmt->close();
    return $tasks;
}

/**
     * Fetching complete details of the caretaker by id 
     * @param String $id id of the caretaker
     */
public function getCareTakerCompleteDetails($id) {
    $stmt = $this->conn->prepare("SELECT *,caretakers.`ID/Address_proof` as id_addressProof FROM `caretakers` WHERE caretakers.id = ".$id);
    
    
       // $stmt->bind_param("i", $user_id['id']);
    $stmt->execute();
    $tasks = $stmt->get_result();
    $stmt->close();
    return $tasks;
}

  /**
     * Fetching all projects under an organisation
     * @param String $organisation_id id of the organisation
     */
  public function getProjectsUnderOrganisation($organisation_id) {
    $stmt = $this->conn->prepare("SELECT projects.id as project_id,projects.age,projects.gender,projects.services,projects.duration,projects.latlong,projects.coated_on,projects.hours,projects.address,projects.weight,users.first_name,users.last_name,users.first_language,users.second_language,users.third_language FROM users JOIN `projects` WHERE projects.caretaker_id IN (SELECT caretakers.id from caretakers where caretakers.organisation_id = ".$organisation_id.") and projects.status = 1 AND projects.user_id = users.id");
    
    
       // $stmt->bind_param("i", $user_id['id']);
    $stmt->execute();
    $tasks = $stmt->get_result();
    $stmt->close();
    return $tasks;
}



    /**
     * Fetching all product
     * @param String $user_id id of the user
     */
    public function getRentProducts($user_id,$type) {
        $stmt = $this->conn->prepare("SELECT *,(SELECT cart.count FROM cart WHERE cart.user_id = ? and cart.product_id = products.id) as item_count FROM `products` WHERE products.stock>0 and products.type =?");
        
        $stmt->bind_param("ss", $user_id['id'],$type);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

 /**
     * Fetching all product
     * @param String $user_id id of the user
     */
 public function getCartProducts($user_id) {
    $stmt = $this->conn->prepare("SELECT products.*,cart.id as cart_id, cart.count as item_count  FROM `cart` INNER JOIN  `products` WHERE products.id = cart.`product_id` and cart.user_id = ? and cart.count>0 ");
    
    $stmt->bind_param("s",$user_id['id']);
    $stmt->execute();
    $tasks = $stmt->get_result();
    $stmt->close();
    return $tasks;
}


 /**
     * Fetching all product
     * @param String $user_id id of the user
     */
 public function deleteItemFromCart($cart_id, $user_id) {
    $stmt = $this->conn->prepare("DELETE FROM `cart` WHERE cart.id = ? and cart.user_id = ?");
    $stmt->bind_param("ss", $cart_id,$user_id['id']);
    $stmt->execute();
    $num_affected_rows = $stmt->affected_rows;
    $stmt->close();
    return $num_affected_rows > 0;
}


    /**
     * Fetching complete details of an organisation
     * @param String $organisation_id id of the organsiation
     */
    public function getCompleteDetailofOrganisation($organisation_id) {
        $stmt = $this->conn->prepare("SELECT * , (SELECT COUNT(projects.id) from projects WHERE projects.caretaker_id IN (SELECT caretakers.id FROM caretakers WHERE caretakers.organisation_id= organisations.id) and projects.status = 1) as ongoing_projects_count,(SELECT COUNT(projects.id) from projects WHERE projects.caretaker_id IN (SELECT caretakers.id FROM caretakers WHERE caretakers.organisation_id= organisations.id) and projects.status = 2) as completed_projects_count FROM `organisations` WHERE id = ?");
        
        $stmt->bind_param("s", $organisation_id);
        $stmt->execute();
        $tasks = $stmt->get_result();
        
        $stmt->close();
        return $tasks;
    }



 /**
     * Function to add item to cart with default count 0
     * @param String $user_id id of the user
     * @param String $product_id id of the product
     */
 public function addItemToCart($user_id, $product_id) {
     

    if($this->checkItemInCart($user_id,$product_id) >0)

    {
     
        $updateResult = $this->updateCartCount($user_id,$product_id);
        

        
        return $updateResult;

    }
    else
    {

        $stmt = $this->conn->prepare("INSERT INTO  `cart`(`user_id`, `product_id`, `count`) values(?, ?,0)");
        $stmt->bind_param("ss", $user_id['id'], $product_id);
        $result = $stmt->execute();
        $stmt->close();
        if($result)
        {
            $updateResult = $this->updateCartCount($user_id,$product_id);
            return $updateResult;
        }
        else
        {
            return null;
            
        }
    }
    
}


 /**
     * Function to create new order
     * @param String $user_id id of the user
     * @param String $payment_type type of payment(0 for cod , 1 for online payment)
     */
 public function createOrder($user_id, $payment_type,$address,$latlong) {
     

  $result = array();

  if($payment_type ==0 )
  {

//status = 1 for confirmed order , 0 for online payment waiting for payment , 2 for cancelled 
      $result = $this->createOrderWithStatus($payment_type,1,$user_id,$address,$latlong);

  }
  else if ($payment_type == 1)
  {
    $result = $this->createOrderWithStatus($payment_type,0,$user_id,$address,$latlong);
}

return $result;

}



public function createOrderWithStatus($payment_type,$status,$user_id,$address,$latlong)
{

   $total_price =  $this->getUserCartSum($user_id);
   $date = date('Y-m-d H:i:s', time());
   
   //  print_r($user_id['id']. $total_price.$payment_type.$date.$status);
   
   $stmt = $this->conn->prepare("INSERT INTO  `orders`(`user_id`, `total_price`, `payment_type`,`created_at`,`status`,`address`,`latlong`) values(?, ?,?,?,?,?,?)");
   $stmt->bind_param("iisssss", $user_id['id'], $total_price,$payment_type,$date,$status,$address,$latlong);
    
   $result = $stmt->execute();
   $new_task_id = $this->conn->insert_id;
   $stmt->close();
   if($result)
   {

    $stmt = $this->conn->prepare("INSERT INTO order_items(order_id, product_id,quantity)
      SELECT ?,cart.product_id, cart.count as quantity FROM cart
      WHERE cart.user_id =?");

    
    $stmt->bind_param("ss", $new_task_id,$user_id['id']);
    $result = $stmt->execute();
    $stmt->close();
    if($result)
    {
      $stmt = $this->conn->prepare("DELETE from cart where cart.user_id = ".$user_id['id']);

      

      
      $result = $stmt->execute();
      $stmt->close();

      if($result)
      {
         $result = array();
         $result['order_id'] = $new_task_id;
         $result['order_amount'] = $total_price;
         return $result; 
     }
     else
        return null;
    
}




}
else
{
 
    return null;
    
}

}


    /**
     * Function to make a new quote by an organisation
     * @param String $caretaker_id id of the caretaker
     * @param String $project_id id of the project
     * @param String $coated_price coated price
     */
    public function projectQuotebyOrganisation($caretaker_id, $project_id,$coated_price) {
     
      
        $stmt = $this->conn->prepare("INSERT INTO  `projectquotes`(`project_id`, `caretaker_id`, `coated_price`) values(?, ?,?)");
        $stmt->bind_param("sss", $project_id,$caretaker_id,$coated_price);
        $result = $stmt->execute();
        $stmt->close();
        if($result)
        {
            $updateResult = $this->updateCareTakerStatus($caretaker_id);
            return $updateResult;
        }
        else
        {
            return null;
            
        }
        
        
    }





    /**
     * Function to check whether item with some count is already there in the cart or not?
     * @param String $user_id id of the user
     * @param String $product_id id of the product
     */
    public function checkItemInCart($user_id, $product_id) {
        $stmt = $this->conn->prepare("SELECT cart.id from cart WHERE cart.product_id = ".$product_id." and cart.user_id = ".$user_id['id']);
        
        
       // $stmt->bind_param("ss", $product_id, $user_id['id']);
        $result = $stmt->execute();
        $tasks = $stmt->get_result();
        
        
        $stmt->close();
        
        return $tasks->num_rows;

        
        
    }


     /**
     * Updating item count in stock along with stock of the item
     * @param String $user_id id of the user
     * @param String $prodict_id id of the product
     *  
     */
     public function updateCartCount($user_id,$product_id) {
        $stmt = $this->conn->prepare("UPDATE cart SET cart.count = cart.count+1 WHERE cart.product_id = ? and cart.user_id = ? and (SELECT products.stock FROM products WHERE products.id = cart.product_id )>0");
        $stmt->bind_param("ss", $product_id, $user_id['id']);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();

      /*  if($num_affected_rows > 0)
        {
             return    $this->updateProductStock($product_id);
        }
        else*/
            return $num_affected_rows > 0;
    }


 /**
     * Updating item count in stock along with stock of the item
     * @param String $user_id id of the user
     * @param String $prodict_id id of the product
     *  
     */
     public function deleteCareTaker($caretaker_id) {
        $stmt = $this->conn->prepare("UPDATE caretakers SET caretakers.status = 0 WHERE caretakers.id = ?");
        $stmt->bind_param("s", $caretaker_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();

      /*  if($num_affected_rows > 0)
        {
             return    $this->updateProductStock($product_id);
        }
        else*/
            return $num_affected_rows > 0;
    }

  /**
     * Updating item count in stock along with stock of the item
     * @param String $user_id id of the user
     * @param String $prodict_id id of the product
     *  
     */
  public function updateQuote($quote_id,$coated_price,$caretaker_id,$oldcareTakerId) {
    $date = date('Y/m/d H:i:s', time());

    $stmt = $this->conn->prepare("update projectquotes set projectquotes.`caretaker_id` =?  ,projectquotes.`coated_price` = ?,projectquotes.`created_at` = ? where projectquotes.id = ?");
    $stmt->bind_param("ssss", $caretaker_id, $coated_price,$date,$quote_id);
    $stmt->execute();
    $num_affected_rows = $stmt->affected_rows;
    $stmt->close();

    if( $num_affected_rows > 0)
    {
       $stmt = $this->conn->prepare("UPDATE caretakers SET caretakers.status = CASE WHEN caretakers.id =  ? THEN  '1' WHEN caretakers.id =  ? THEN  '2' else caretakers.status END");
       $stmt->bind_param("ss", $oldcareTakerId,$caretaker_id);
       $stmt->execute();
       $num_affected_rows = $stmt->affected_rows;
       $stmt->close();
       
       return $num_affected_rows > 0;

   }
   else
   {
       return $num_affected_rows > 0;
   }

   
}




  /**
     * Updating item count in stock along with stock of the item
     * @param String $user_id id of the user
     * @param String $prodict_id id of the product
     *  
     */
  public function cancelQuote($quote_id,$caretaker_id) {
    $date = date('Y/m/d H:i:s', time());

    $stmt = $this->conn->prepare("update projectquotes set projectquotes.`coated_price` = -1,projectquotes.`created_at` = ? where projectquotes.id = ? and projectquotes.caretaker_id = ?");
    $stmt->bind_param("sss",  $date,$quote_id,$caretaker_id);
    $stmt->execute();
    $num_affected_rows = $stmt->affected_rows;
    $stmt->close();

    
    if($num_affected_rows > 0)
    {
      $stmt = $this->conn->prepare("update caretakers set caretakers.status = 1 where caretakers.id = ?");
      $stmt->bind_param("s",  $caretaker_id);
      $stmt->execute();
      $num_affected_rows = $stmt->affected_rows;
      $stmt->close();

      return $num_affected_rows > 0;
  }

  else
    return $num_affected_rows > 0;
}



  /**
     * Updating user fcm_id
     * @param String $user_id id of the user
     * @param String $fcm_id fcm id of the user
     *  
     */
  public function updateUserFCMID($user_id,$fcm_id) {
    $stmt = $this->conn->prepare("update users SET users.fcm_id = ? WHERE users.id = ?");
    $stmt->bind_param("ss", $fcm_id, $user_id['id']);
    $stmt->execute();
    $num_affected_rows = $stmt->affected_rows;
    $stmt->close();

      /*  if($num_affected_rows > 0)
        {
             return    $this->updateProductStock($product_id);
        }
        else*/
            return $num_affected_rows > 0;
    }


    /**
     * Updating organisation fcm_id
     * @param String $organisation_id id of the user
     * @param String $fcm_id fcm id of the user
     *  
     */
    public function updateOrganisationFcmID($organisation_id,$fcm_id) {
        $stmt = $this->conn->prepare("UPDATE organisations SET organisations.fcm_id = ? WHERE organisations.id = ?");
        $stmt->bind_param("ss", $fcm_id, $organisation_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();

      /*  if($num_affected_rows > 0)
        {
             return    $this->updateProductStock($product_id);
        }
        else*/
            return $num_affected_rows > 0;
    }

     /**
     * Updating organisation fcm_id
     * @param String $organisation_id id of the user
     * @param String $fcm_id fcm id of the user
     *  
     */
    public function updateDoctorFcmID($doctor_id,$fcm_id) {
        $stmt = $this->conn->prepare("UPDATE doctors SET doctors.fcm_id = ? WHERE doctors.id = ?");
        $stmt->bind_param("ss", $fcm_id, $doctor_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();

      /*  if($num_affected_rows > 0)
        {
             return    $this->updateProductStock($product_id);
        }
        else*/
            return $num_affected_rows > 0;
    }
     /**
     * Updating item count in stock along with stock of the item
     * @param String $user_id id of the user
     * @param String $prodict_id id of the product
     *  
     */
     public function updateCareTakerStatus($caretaker_id) {
        $stmt = $this->conn->prepare("update caretakers SET caretakers.status = 2 WHERE caretakers.id = ?");
        $stmt->bind_param("s",  $caretaker_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();

        return $num_affected_rows > 0;
    }


    /**
     * Updating item count in stock along with stock of the item
     * @param String $user_id id of the user
     * @param String $prodict_id id of the product
     *  
     */
    public function updateCartCountbyRemovingOneItem($user_id,$product_id) {
        $stmt = $this->conn->prepare("UPDATE cart SET cart.count = (cart.count-1) WHERE cart.count>0 and cart.product_id = ? and cart.user_id = ?");
        $stmt->bind_param("ss", $product_id, $user_id['id']);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }


 /**
     * Updating item count in stock along with stock of the item
     * @param String $user_id id of the user
     * @param String $prodict_id id of the product
     *  
     */
 public function updateProductStock($product_id) {
    $stmt = $this->conn->prepare("UPDATE products SEt products.stock = (products.stock-1) WHERE products.id = ?");
    $stmt->bind_param("s", $product_id);
    $stmt->execute();
    $num_affected_rows = $stmt->affected_rows;
    $stmt->close();
    return $num_affected_rows > 0;
}


     /**
     * Fetching all projects finished by an organisation
     * @param String $organisation_id id of the organisation
     */
     public function getCompletedProjectsOfUser($user_id) {

       
        $stmt = $this->conn->prepare("SELECT projects.id as project_id,projects.age,projects.gender,projects.services,projects.duration,projects.latlong,projects.coated_on,projects.hours,projects.address,projects.weight,users.first_name,users.last_name FROM users JOIN `projects` WHERE  projects.status = 2 AND users.id = ".$user_id['id']." AND projects.user_id =".$user_id['id'] );
        
        
       // $stmt->bind_param("i", $user_id['id']);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }





     /**
     * Fetching all projects finished by an organisation
     * @param String $organisation_id id of the organisation
     */
     public function getProjectsOfOrganisation($organisation_id) {
        $stmt = $this->conn->prepare("SELECT projects.id as project_id,projects.age,projects.gender,projects.services,projects.duration,projects.latlong,projects.coated_on,projects.hours,projects.address,projects.weight,users.first_name,users.last_name,users.first_language,users.second_language,users.third_language FROM users JOIN `projects` WHERE projects.caretaker_id IN (SELECT caretakers.id from caretakers where caretakers.organisation_id = ".$organisation_id.") and projects.status = 2 AND projects.user_id = users.id");
        
        
       // $stmt->bind_param("i", $user_id['id']);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }



     /**
     * Fetching all projects which needs caretakers
     *  
     */
     public function getPendingProjectForOrganisation($organisation_id) {
        $stmt = $this->conn->prepare("SELECT projects.id as project_id, projects.age,projects.gender,projects.services,projects.duration,projects.latlong,projects.weight,projects.address,users.first_name,users.last_name,users.first_language,users.second_language,users.third_language FROM users JOIN `projects` WHERE projects.status = 0 AND projects.user_id = users.id and projects.id NOT in (SELECT projectquotes.project_id from projectquotes WHERE projectquotes.caretaker_id IN(SELECT caretakers.id FROM caretakers WHERE caretakers.organisation_id = ?))");
        
        
        $stmt->bind_param("s", $organisation_id);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

 /**
     * Updating caretaker details
     * @param String $id id of the caretaker
     * @param String $name name of the care taker
     * @param String $email email of the care taker
     * @param String $mobile mobile of the care taker
     * @param String $gender gender of the care taker
     */
 public function updateCareTakerDetails($id, $name,$email,$mobile, $gender,$age,$doj,$qualification,$experience,$designation,$addressproof,$blood_group,$martial_status,$residence_number,$father,$mother,$spouse,$children,$background_verification,$address) {
    $stmt = $this->conn->prepare("UPDATE `caretakers` SET `caretaker_name`=?,`email`=?,`mobile`=?,`gender`=?,`address`=?,`age`=?,`DOJ`=?,`qualification`=?,`experience`=?,`designation`=?,`residence_number`=?,`ID/Address_proof`=?,`blood_group`=?,`father`=?,`mother`=?,`martial_status`=?,`spouse`=?,`children`=?,`background_verification`=? WHERE caretakers.id = ?");
    
    $stmt->bind_param("ssssssssssssssssssss", $name, $email, $mobile, $gender,$address,$age,$doj,$qualification,$experience,$designation,$residence_number,$addressproof,$blood_group,$father,$mother,$martial_status,$spouse,$children,$background_verification,$id);

   
    $result = $stmt->execute();
    $stmt->close();
    return $result;
}


    /**
     * Updating task
     * @param String $task_id id of the task
     * @param String $task task text
     * @param String $status task status
     */
    public function updateTask($user_id, $task_id, $task, $status) {
        $stmt = $this->conn->prepare("UPDATE tasks t, user_tasks ut set t.task = ?, t.status = ? WHERE t.id = ? AND t.id = ut.task_id AND ut.user_id = ?");
        $stmt->bind_param("siii", $task, $status, $task_id, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

/**
     * updating ProjectCompletion
     * @param String $project_id id of the project
     * @param String $user_id id of the user
     *  
     */
public function updateProjectCompletion($project_id) {
    $stmt = $this->conn->prepare("update projects set projects.status =2 where projects.id = ?");
    $stmt->bind_param("s", $project_id);
    $stmt->execute();
    $num_affected_rows = $stmt->affected_rows;
    $stmt->close();
    if($num_affected_rows > 0)
    {
       $stmt = $this->conn->prepare("update caretakers set caretakers.`status` = 1 where id = (select projects.caretaker_id from projects where projects.id = ".$project_id.")");
       $result = $stmt->execute();
        
       $stmt->close();
       return $result;
   }
   else
   {
       return $num_affected_rows > 0;
   }
}




     /**
     * Book a care taker
     * @param String $project_id id of the project
     * @param String $caretaker_id id of the care taker
     * @param String $coated_price price been coated
     */
     public function bookCareTaker($project_id, $caretaker_id, $coated_price) {
        $stmt = $this->conn->prepare("UPDATE projects set projects.coated_on = Now(),projects.caretaker_id =".$caretaker_id." ,projects.coated_price = ".$coated_price." , projects.status = 1 where projects.id = ".$project_id." and projects.status = 0");


        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();

        if($num_affected_rows > 0)
        {
           $stmt = $this->conn->prepare("UPDATE caretakers SET caretakers.status =1 WHERE caretakers.id IN ( SELECT projectquotes.caretaker_id FROM projectquotes WHERE projectquotes.project_id =".$project_id." AND projectquotes.caretaker_id !=".$caretaker_id." )");
           
  
          $result =  $stmt->execute();

          // $num_affected_rows = $stmt->affected_rows;
           $stmt->close();
           return $result;
       }
       else
       {
           return $num_affected_rows > 0;
       } 
 //return $num_affected_rows > 0;

       
   }

 /**
     * update user details
     * @param String $user_id id of the user
     * @param String $first_name first_name of the care user
     * @param String $last_name last_name of the user
     * @param String $email email of the user
     * @param String $mobile mobile of the user
     */
 public function updateUserDetails($user_id, $first_name, $last_name,$email,$mobile,$first_lanugage,$second_language,$third_language) {
    $stmt = $this->conn->prepare("update users set first_name='".$first_name."',last_name ='".$last_name."',email ='"
        .$email."',mobile ='".$mobile."',first_language='".$first_lanugage."',second_language='".$second_language."',third_language='".$third_language."' WHERE id = ".$user_id['id']);
    
    $result = $stmt->execute();
    //$num_affected_rows = $stmt->affected_rows;
    $stmt->close();
    return $result;
}



 /**
     * update user details
     * @param String $user_id id of the user
     * @param String $first_name first_name of the care user
     * @param String $last_name last_name of the user
     * @param String $email email of the user
     * @param String $mobile mobile of the user
     */
 public function updateDoctorDetails($name, $age,$email,$mobile,$gender,$location,$latlong,$specialiation,$experience,$additional_degree,$time_slots,$id) {
    $stmt = $this->conn->prepare("UPDATE `doctors` SET `name`='".$name."',`email`='".$email."',`mobile`='".$mobile."',`age`='".$age."',`gender`='".$gender."',`location`='".$location."',`latlong`='".$latlong."',`specialization`='".$specialiation."',`experience`='".$experience."',`additional_degree`='".$additional_degree."',`timeslots`='".$time_slots."' WHERE id = ".$id);
    

    $result = $stmt->execute();
    //$num_affected_rows = $stmt->affected_rows;
    $stmt->close();
    return $result;
}




    /**
     * Deleting a task
     * @param String $task_id id of the task to delete
     */
    public function deleteTask($user_id, $task_id) {
        $stmt = $this->conn->prepare("DELETE t FROM tasks t, user_tasks ut WHERE t.id = ? AND ut.task_id = t.id AND ut.user_id = ?");
        $stmt->bind_param("ii", $task_id, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    /* ------------- `user_tasks` table method ------------------ */

    /**
     * Function to assign a task to user
     * @param String $user_id id of the user
     * @param String $task_id id of the task
     */
    public function createUserTask($user_id, $task_id) {
        $stmt = $this->conn->prepare("INSERT INTO user_tasks(user_id, task_id) values(?, ?)");
        $stmt->bind_param("ii", $user_id, $task_id);
        $result = $stmt->execute();
        $stmt->close();
        return $result;
    }



    /*---------------------website methods-----------------*/



 /**
     * Creating new task
     * @param String $user_id user id to whom task belongs to
     * @param String $task task text
     */
 public function bookDoctorAppointmentFromWebsite($doctor_id,$user_id,$actual_time_slot,$time_slot,$appointment_date,$created_at,$latlong,$address) {        
     
       // print_r($user_id);
    $stmt = $this->conn->prepare("INSERT INTO `appointments`( `doctor_id`, `user_id`, `date`, `timeslot`, `timeslot_sub`, `created_at`,`latlong`,`address`) VALUES (?,?,?,?,?,?,?,?)");
    $stmt->bind_param("ssssssss", $doctor_id,$user_id,$appointment_date,$actual_time_slot,$time_slot,$created_at,$latlong,$address);
    $result = $stmt->execute();
    

    if ($result) {
            // task row created
            // now assign the task to user
        $new_task_id = $this->conn->insert_id;
        $stmt->close();
           return $new_task_id;
        
    } else {
            // task failed to create
        echo($stmt->error);
        $stmt->close();
        return NULL;
    }
}




public function fetchDotorPastAppointments($doctor_id,$appointment_date)
{

  $stmt = $this->conn->prepare("SELECT *, (SELECT concat(users.first_name,' ',users.last_name) FROM users WHERE users.api_key = appointments.user_id) as user_name FROM `appointments` WHERE appointments.date <= ? and status!=0 and doctor_id = ? ");
        

        $stmt->bind_param("ss", $appointment_date,$doctor_id);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks; 

}


public function fetchDotorAppointments($doctor_id,$appointment_date)
{

  $stmt = $this->conn->prepare("SELECT *, (SELECT concat(users.first_name,' ',users.last_name) FROM users WHERE users.api_key = appointments.user_id) as user_name FROM `appointments` WHERE appointments.date = ? and status = 0 and doctor_id = ? ");
        

        $stmt->bind_param("ss", $appointment_date,$doctor_id);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks; 

}

    public function fetchAvailableDoctors($appointment_date,$user_id)
    {

        $stmt = $this->conn->prepare("SELECT doctors.*, ((CHAR_LENGTH(timeslots) - (CHAR_LENGTH(REPLACE(timeslots,',',''))))+1)*8 as total_appointments,((SELECT COUNT(*) FROM appointments WHERE appointments.doctor_id = doctors.id and appointments.date = ?)) as actual_appointments FROM `doctors` HAVING total_appointments>actual_appointments ");
        
        
        $stmt->bind_param("s", $appointment_date);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks; 
    }

     public function fetchAvailableSlots($appointment_date,$doctor_id)
    {

        $stmt = $this->conn->prepare("SELECT * FROM `appointments` WHERE date =? and appointments.doctor_id = ?");
        
        
        $stmt->bind_param("ss", $appointment_date,$doctor_id);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        
        return $tasks; 
    }


     public function fetchAllSlotsOfDoctors($doctor_id)
    {

        $stmt = $this->conn->prepare("SELECT timeslots FROM `doctors` WHERE doctors.id = ?");
        
        
        $stmt->bind_param("s", $doctor_id);
        $stmt->execute();
        $tasks = $stmt->get_result()->fetch_assoc();
        $stmt->close();
        return $tasks; 
    }


    public function cancelDoctorAppointment($doctor_id,$appointment_id)
    {

         $stmt = $this->conn->prepare("UPDATE appointments  set status = '1' where id = ? and doctor_id = ?");


    $stmt->bind_param("ss", $appointment_id, $doctor_id);
    $stmt->execute();
    $num_affected_rows = $stmt->affected_rows;
    $stmt->close();
    return $num_affected_rows > 0;
    }


    public function fetchFcmIDOfDoctors($doctor_id)
    {

        $stmt = $this->conn->prepare("SELECT fcm_id FROM `doctors` WHERE doctors.id = ?");
        
        
        $stmt->bind_param("s", $doctor_id);
        $stmt->execute();
        $tasks = $stmt->get_result()->fetch_assoc();
        $stmt->close();
        return $tasks; 
    }

 

}

?>