<?php
date_default_timezone_set("Asia/Kolkata");
$req_dump = print_r($_REQUEST, TRUE);
$fp = fopen('request.log', 'a');
fwrite($fp, $req_dump);
fclose($fp);  


error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once '../firebase/push.php';
require_once '../include/DbHandler.php';
require_once '../include/PassHash.php';
require_once '../firebase/firebase.php';
require_once '../phpmailer/PHPMailerAutoload.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// User id from db - Global Variable
$user_id = NULL;

/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route) {
    // Getting request headers
  $headers = apache_request_headers();
  $response = array();
  $app = \Slim\Slim::getInstance();


    // Verifying Authorization Header
  if (isset($_POST['Authorization'])) {
    $db = new DbHandler();

        // get the api key
    $api_key = $_POST['Authorization'];
        // validating api key
    if (!$db->isValidApiKey($api_key)) {
            // api key is not present in users table
      $response["error"] = true;
      $response["message"] = "Access Denied. Invalid Api key";
      echoRespnse(401, $response);
      $app->stop();
    } else {
      global $user_id;
            // get user primary key id
      $user_id = $db->getUserId($api_key);
    }
  } 

  else   if (isset($_GET['Authorization'])) {
    $db = new DbHandler();

        // get the api key
    $api_key = $_GET['Authorization'];
        // validating api key
    if (!$db->isValidApiKey($api_key)) {
            // api key is not present in users table
      $response["error"] = true;
      $response["message"] = "Access Denied. Invalid Api key";
      echoRespnse(401, $response);
      $app->stop();
    } else {
      global $user_id;
            // get user primary key id
      $user_id = $db->getUserId($api_key);
    }
  } 





  else {
        // api key is missing in header
    $response["error"] = true;
    $response["message"] = "Api key is misssing";
    echoRespnse(400, $response);
    $app->stop();
  }
}

/**
 * ----------- METHODS WITHOUT AUTHENTICATION ---------------------------------
 */


/**
     * Creating new task in db
     * method POST
     * params - name
     * url - /tasks/
     */
$app->post('/contactus', 'authenticate', function() use ($app) {
                // check for required params
  verifyRequiredParams(array('type','message'));

  $response = array();
  $type = $app->request->post('type');
  $message = $app->request->post('message');

  global $user_id;
  $db = new DbHandler();

                // creating new task
  $task_id = $db->createContactus($type,$message,$user_id);

  if ($task_id != NULL) {

    if(isset($_FILES['uploaded_file'])&& $_FILES["uploaded_file"]["error"] == 0)
    {
      $file_path = "images/";

      $file_path = $file_path . basename( $_FILES['uploaded_file']['name']);

      try

      {
        if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $file_path)) {

         $result = $db->updateAttachmentUrl($file_path,$task_id);
         if ($result) {
                // task updated successfully
          $response["error"] = false;
          $response["message"] = "Thanks for your support";
          echoRespnse(201, $response);
        } else {
                // task failed to update
         $response["error"] = false;
         $response["message"] = "Thanks for your support";
         echoRespnse(201, $response);

       }

     }
     else
     {
      $response["error"] = false;
      $response["message"] = "Thanks for your support";
      echoRespnse(201, $response);
    } 
  }

  catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
  }



}
else
{
 $response["error"] = false;
 $response["message"] = "Thanks for your supportss";
 echoRespnse(201, $response);
}


}
else
{
 $response["error"] = true;
 $response["message"] = "Something went wrong";
 echoRespnse(201, $response);
}

});



/**
 * User Registration
 * url - /register
 * method - POST
 * params - name, email, password
 */
$app->post('/createOrder', 'authenticate',function() use ($app) {



 global $user_id;
            // check for required params
 verifyRequiredParams(array('payment_type','address','latlong'));

 $response = array();

            // reading post params

 $payment_type = $app->request->post('payment_type');
 $address = $app->request->post('address');
 $latlong = $app->request->post('latlong');


 
 $db = new DbHandler();
 $res = $db->createOrder($user_id, $payment_type,$address,$latlong);

 if ($res != NULL) {
  $response= $res;
  $response["error"] = false;
  $response["message"] = "You are order placed successfully";
} else if ($res == USER_CREATE_FAILED) {
  $response["error"] = true;
  $response["message"] = "Oops! An error occurred while registereing";
} else if ($res == USER_ALREADY_EXISTED) {
  $response["error"] = true;
  $response["message"] = "Sorry, this email already existed";
}
            // echo json response
echoRespnse(201, $response);
});


/**
 * User Registration
 * url - /register
 * method - POST
 * params - name, email, password
 */
$app->post('/register', function() use ($app) {
            // check for required params
  verifyRequiredParams(array( 'email', 'password','mobile','first_name','last_name','first_languge'));

  $response = array();

            // reading post params

  $email = $app->request->post('email');
  $mobile = $app->request->post('mobile');
  $first_name = $app->request->post('first_name');
  $last_name = $app->request->post('last_name');
  $password = $app->request->post('password');
  $first_languge= $app->request->post('first_languge');
  $second_languge= $app->request->post('second_languge');
  $third_languge= $app->request->post('third_languge');

  if(is_null($second_languge))

    $second_languge= "";


  if(is_null($third_languge))

    $third_languge= "";

            // validating email address
  validateEmail($email);

  $db = new DbHandler();
  $res = $db->createUser($first_name, $last_name,$email,$mobile, $password,$first_languge,$second_languge,$third_languge);

  if ($res == USER_CREATED_SUCCESSFULLY) {

   sendRegisterMailtoUser($first_name." ".$last_name,$email);
   if(isset($_FILES['uploaded_file'])&& $_FILES["uploaded_file"]["error"] == 0)
   {



    $file_path = "images/";

    $file_path = $file_path . basename( $_FILES['uploaded_file']['name']);

    if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $file_path)) {

     $result = $db->updateProfilePicPath($file_path, $email);
     if ($result) {
                // task updated successfully
      $response["error"] = false;
      $response["message"] = "You are successfully registered";
    } else {
                // task failed to update
     $response["error"] = false;
     $response["message"] = "You are successfully registered but profile picture need to be uploaded";
   }

 }
 else
 {
  $response["error"] = false;
  $response["message"] = "You are successfully registered but profile picture need to be uploaded";
}


} else{

  $response["error"] = false;
  $response["message"] = "You are successfully registered but profile picture need to be uploaded";
}


} else if ($res == USER_CREATE_FAILED) {
  $response["error"] = true;
  $response["message"] = "Oops! An error occurred while registereing";
} else if ($res == USER_ALREADY_EXISTED) {
  $response["error"] = true;
  $response["message"] = "Sorry, this email already existed";
}
            // echo json response
echoRespnse(200, $response);
});



/**
 * Doctor Registration
 * url - /doctorregister
 * method - POST
 * params - name, email, password
 */
$app->post('/doctorregister', function() use ($app) {
            // check for required params
  verifyRequiredParams(array( 'email', 'password','mobile','name','age','gender','location','latlong','specialiation','experience','time_slots'));

  $response = array();

            // reading post params

  $email = $app->request->post('email');
  $mobile = $app->request->post('mobile');
  $name= $app->request->post('name');
  $last_name = $app->request->post('last_name');
  $password = $app->request->post('password');
  $age= $app->request->post('age');
  $gender= $app->request->post('gender');
  $location= $app->request->post('location');
  $latlong= $app->request->post('latlong');
  $specialiation= $app->request->post('specialiation');
  $experience= $app->request->post('experience');
  $additional_degree= $app->request->post('additional_degree');
  $time_slots= $app->request->post('time_slots');

  if(is_null($email ))

    $email = "";


  if(is_null($mobile ))

    $mobile = "";

  if(is_null($additional_degree))

    $additional_degree= "";



            // validating email address
  validateEmail($email);

  $db = new DbHandler();
  $res = $db->createDoctor($name, $age,$email,$mobile, $password,$gender,$location,$latlong,$specialiation,$experience,$additional_degree,$time_slots);

  if ($res == USER_CREATED_SUCCESSFULLY) {

    if(isset($_FILES['uploaded_file'])&& $_FILES["uploaded_file"]["error"] == 0 )
    {
      $file_path = "images/";

      $file_path = $file_path . basename( $_FILES['uploaded_file']['name']);

      if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $file_path)) {

       $result = $db->updateProfilePicPathOfDoctor($file_path, $email);
       if ($result) {
                // task updated successfully
        $response["error"] = false;
        $response["message"] = "You are successfully registered";
      } else {
                // task failed to update
       $response["error"] = false;
       $response["message"] = "You are successfully registered but profile picture need to be uploaded";
     }

   }
   else
   {
    $response["error"] = false;
    $response["message"] = "You are successfully registered but profile picture need to be uploaded";
  }


} else{

  $response["error"] = false;
  $response["message"] = "You are successfully registered but profile picture need to be uploaded";
}


} else if ($res == USER_CREATE_FAILED) {
  $response["error"] = true;
  $response["message"] = "Oops! An error occurred while registereing";
} else if ($res == USER_ALREADY_EXISTED) {
  $response["error"] = true;
  $response["message"] = "Sorry, this email already existed";
}
            // echo json response
echoRespnse(200, $response);
});



/**
 * User Registration
 * url - /register
 * method - POST
 * params - name, email, password
 */
$app->post('/createCareTaker', function() use ($app) {
            // check for required params
  verifyRequiredParams(array('gender','name','organisation_id','age','experience','designation','addressproof','blood_group','martial_status','address','height','weight','languages'));

  $response = array();

            // reading post params


  $email = $app->request->post('email');
  $mobile = $app->request->post('mobile');
  $name = $app->request->post('name');
  $gender = $app->request->post('gender');
  $organisation_id = $app->request->post('organisation_id');
  $age = $app->request->post('age');
  $doj = $app->request->post('doj');
  $qualification = $app->request->post('qualification');
  $experience = $app->request->post('experience');
  $designation = $app->request->post('designation');
  $addressproof = $app->request->post('addressproof');
  $blood_group = $app->request->post('blood_group');
  $martial_status = $app->request->post('martial_status');
  $residence_number = $app->request->post('residence_number');
  $father = $app->request->post('father');
  $mother = $app->request->post('mother');
  $spouse = $app->request->post('spouse');
  $children = $app->request->post('children');
  $background_verification = $app->request->post('background_verification');
  $address = $app->request->post('address');
$height = $app->request->post('height');
$weight = $app->request->post('weight');
$languages = $app->request->post('languages');
  if(is_null($residence_number))

    $residence_number = "";

  if(is_null($father))
    $father="";
  if(is_null($mother))
    $mother = "";
  if(is_null($spouse))
    $spouse="";
  if(is_null($children))
    $children="";

  if(is_null($email))
    $email="";

  if(is_null($mobile))
    $mobile="";

  if(is_null($doj))
    $doj="";

  if(is_null($qualification))
    $qualification="";

  if(is_null($background_verification))
    $background_verification="";
            // validating email address


  $db = new DbHandler();
  $result = $db->createCareTaker($organisation_id, $name,$email,$mobile, $gender,$age,$doj,$qualification,$experience,$designation,$addressproof,$blood_group,$martial_status,$residence_number,$father,$mother,$spouse,$children,$background_verification,$address,$height,$weight,$languages);
    $res = $result["result"];
    $insert_id = $result["id"];
    
  if ($res == USER_CREATED_SUCCESSFULLY) {
    $file_path = "images/";

    $file_path = $file_path . basename( $_FILES['uploaded_file']['name']);

    if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $file_path)) {

     $result = $db->updateProfilePicPathOfCaretaker($file_path, $insert_id);
     if ($result) {
                // task updated successfully
      $response["error"] = false;
      $response["id"] = $insert_id;
      $response["message"] = "You are successfully registered,DoDo team will catch you soon";
    } else {
                // task failed to update
     $response["error"] = false;
      $response["id"] = $insert_id;
     $response["message"] = "You are successfully registered but profile picture need to be uploaded";
   }


 } else{

  $response["error"] = false;
   $response["id"] = $insert_id;
  $response["message"] = "You are successfully registered but profile picture need to be uploaded";
} 
}
else if ($res == USER_CREATE_FAILED) {
  $response["error"] = true;
  $response["message"] = "Oops! An error occurred while registereing";
} else if ($res == USER_ALREADY_EXISTED) {
  $response["error"] = true;
  $response["message"] = "Sorry, this mobile already existed";
}
            // echo json response
echoRespnse(201, $response);
});

/**
 * User Registration
 * url - /register
 * method - POST
 * params - name, email, password
 */
$app->post('/createOrganisation', function() use ($app) {
            // check for required params
  verifyRequiredParams(array( 'mobile','email','name','concerned_person','type'));

  $response = array();

            // reading post params

  $email = $app->request->post('email');
  $mobile = $app->request->post('mobile');
  $name = $app->request->post('name');
  $concerned_person = $app->request->post('concerned_person');
  $password = $app->request->post('password');
  $message = $app->request->post('message');
  $type= $app->request->post('type');

  if(is_null($message))
    $message = "";

  if(is_null($password))
    $password = "";
            // validating email address
  validateEmail($email);

  $db = new DbHandler();
  $res = $db->createOrganisation($name, $concerned_person,$email,$password,$mobile,$message,
    $type);

  if ($res == USER_CREATED_SUCCESSFULLY) {
    $response["error"] = false;
    $response["message"] = "You are successfully registered,DoDo team will catch you soon";
  } else if ($res == USER_CREATE_FAILED) {
    $response["error"] = true;
    $response["message"] = "Oops! An error occurred while registereing";
  } else if ($res == USER_ALREADY_EXISTED) {
    $response["error"] = true;
    $response["message"] = "Sorry, this email already existed";
  }
            // echo json response
  echoRespnse(201, $response);
});



/**
 * User Login
 * url - /login
 * method - POST
 * params - email, password
 */
$app->post('/login', function() use ($app) {
            // check for required params


  verifyRequiredParams(array('email', 'password'));

            // reading post params
  $email = $app->request()->post('email');
  $password = $app->request()->post('password');
  $response = array();

  $db = new DbHandler();
            // check for correct email and password
  if ($db->checkLogin($email, $password)) {
                // get the user by email
    $user = $db->getUserByEmail($email);

    if ($user != NULL) {
     $response = $user;
     $response["error"] = false;   
   } else {
                    // unknown error occurred
    $response['error'] = true;
    $response['message'] = "An error occurred. Please try again";
  }
} else {
                // user credentials are wrong
  $response['error'] = true;
  $response['message'] = 'Login failed. Incorrect credentials';
}

echoRespnse(200, $response);
});



/**
 * User Login
 * url - /login
 * method - POST
 * params - email, password
 */
$app->post('/applycoupon','authenticate', function() use ($app) {
            // check for required params

 global $user_id;
 verifyRequiredParams(array('totalprice', 'coupon'));

            // reading post params
 $totalprice = $app->request()->post('totalprice');
 $coupon = $app->request()->post('coupon');
 $response = array();

 $db = new DbHandler();

                // get the user by email
 $user = $db->fetchCouponDetails($user_id,$coupon);

 if ($user != NULL) {



   if($user['type'] == "percentage")
   {


     if($user['min_price']<= $totalprice)
     {

      $user['discount_price'] = PercentageDiscountCalculator($user['discount_price'],$totalprice);
       $user['totalprice'] =$totalprice - $user['discount_price']; 
       $response = $user;
       $response["error"] = false;   
     }


     else
     {
      $response['error'] = true;
      $response['message'] = "Minimum amount to use this coupon is ".$user['min_price'];
    }


  }

  else if($user['type'] == "discount")
  {


   if($user['min_price']<= $totalprice)
   {
     $user['totalprice'] = $totalprice - $user['discount_price'];
     $response = $user;
     $response["error"] = false;   
   }


   else
   {
    $response['error'] = true;
    $response['message'] = "Minimum amount to use this coupon is ".$user['min_price'];
  }


}



} else {
                    // unknown error occurred
  $response['error'] = true;
  $response['message'] = "Coupon doesn't exisits/ coupon limit exceeded";
}

echoRespnse(200, $response);
});



/**
 * Organsiation Login
 * url - /login
 * method - POST
 * params - email, password
 */
$app->post('/organisationLogin', function() use ($app) {
            // check for required params


  verifyRequiredParams(array('email', 'password'));

            // reading post params
  $email = $app->request()->post('email');
  $password = $app->request()->post('password');
  $response = array();

  $db = new DbHandler();
            // check for correct email and password
  if ($db->checkOrganisationLogin($email, $password)) {
                // get the user by email
    $user = $db->getOrganisationDetailsbyEmail($email);

    if ($user != NULL) {

          /*  $response['name'] = $user['name'];
            $response['concerened_person'] = $user['concerened_person'];
            $response['email'] = $user['email'];
            $response['id'] = $user['id'];
            $response['mobile'] = $user['mobile'];
            $response['createdAt'] = $user['created_at'];*/
            $response = $user;
            $response["error"] = false;
          } else {
                    // unknown error occurred
            $response['error'] = true;
            $response['message'] = "An error occurred. Please try again";
          }
        } else {
                // user credentials are wrong
          $response['error'] = true;
          $response['message'] = 'Login failed. Incorrect credentials';
        }

        echoRespnse(200, $response);
      });


/**
 * Organsiation Login
 * url - /login
 * method - POST
 * params - email, password
 */
$app->post('/doctorlogin', function() use ($app) {
            // check for required params


  verifyRequiredParams(array('email', 'password'));

            // reading post params
  $email = $app->request()->post('email');
  $password = $app->request()->post('password');
  $response = array();

  $db = new DbHandler();
            // check for correct email and password
  if ($db->checkDoctorLogin($email, $password)) {
                // get the user by email
    $user = $db->getDoctorByEmail($email);

    if ($user != NULL) {

          /*  $response['name'] = $user['name'];
            $response['concerened_person'] = $user['concerened_person'];
            $response['email'] = $user['email'];
            $response['id'] = $user['id'];
            $response['mobile'] = $user['mobile'];
            $response['createdAt'] = $user['created_at'];*/
            $response = $user;
            $response["error"] = false;
          } else {
                    // unknown error occurred
            $response['error'] = true;
            $response['message'] = "An error occurred. Please try again";
          }
        } else {
                // user credentials are wrong
          $response['error'] = true;
          $response['message'] = 'Login failed. Incorrect credentials';
        }

        echoRespnse(200, $response);
      });




/*
 * ------------------------ METHODS WITH AUTHENTICATION ------------------------
 */

/**
 * Listing all tasks of particual user
 * method GET
 * url /ongoingprojects          
 */
$app->get('/ongoingprojects', 'authenticate', function() {



  global $user_id;
  $response = array();
  $db = new DbHandler();

            // fetching all user tasks
  $result = $db->getAllUserTasks($user_id);

  $response["error"] = false;
  $response["tasks"] = array();

            // looping through result and preparing tasks array
  while ($task = $result->fetch_assoc()) {
    $tmp = $task;
    array_push($response["tasks"], $tmp);
  } 
  echoRespnse(200, $response);
});


/**
 * User Login
 * url - /login
 * method - POST
 * params - email, password
 */
$app->post('/getUserCartCount', 'authenticate',function() use ($app) {
            // check for required params

  global $user_id;


            // reading post params

  $response = array();

  $db = new DbHandler();
            // check for correct email and password

  $user = $db->getUserCartCount($user_id);

  if ($user != NULL) {

    $response = $user;
    $response["error"] = false;
  } else {
                    // unknown error occurred
    $response['error'] = true;
    $response['message'] = "An error occurred. Please try again";
  }


  echoRespnse(200, $response);
});


/**
 * Listing all tasks of particual user
 * method GET
 * url /ongoingprojects          
 */
$app->get('/getCompletedProjectsOfUser', 'authenticate', function() {



  global $user_id;
  $response = array();
  $db = new DbHandler();

            // fetching all user tasks
  $result = $db->getCompletedProjectsOfUser($user_id);

  $response["error"] = false;
  $response["tasks"] = array();

            // looping through result and preparing tasks array
  while ($task = $result->fetch_assoc()) {
    $tmp = $task;
    array_push($response["tasks"], $tmp);
  } 


  echoRespnse(200, $response);
});



/**
 * Listing all tasks of particual user
 * method GET
 * url /ongoingprojects          
 */
$app->get('/getQuotedProjectsByOrganisation', function() use ($app) {



 verifyRequiredParams(array('organisation_id'));
 $organisation_id= $app->request->get('organisation_id');
 $response = array();
 $db = new DbHandler();

            // fetching all user tasks
 $result = $db->getQuotedProjectsByOrganisation($organisation_id);

 $response["error"] = false;
 $response["tasks"] = array();

            // looping through result and preparing tasks array
 while ($task = $result->fetch_assoc()) {
  $tmp = $task;
  array_push($response["tasks"], $tmp);
} 


echoRespnse(200, $response);
});



/**
 * Listing all tasks of particual user
 * method GET
 * url /ongoingprojects          
 */
$app->get('/getDoctorsSpecilizations', function() use ($app) {




  $response = array();
  $db = new DbHandler();

            // fetching all user tasks
  $user = $db->getDoctorsSpecilizations();

  if ($user != NULL) {

    $response = $user;
    $response["error"] = false;
  } else {
                    // unknown error occurred
    $response['error'] = true;
    $response['message'] = "An error occurred. Please try again";
  }


  echoRespnse(200, $response);
});


/**
 * Listing all tasks of particual user
 * method GET
 * url /ongoingprojects          
 */
$app->get('/getCartProducts', 'authenticate', function() use ($app)  {



  global $user_id;
  $response = array();
  $db = new DbHandler();
  $latlong = $app->request->get('latlong');

  if(is_null($latlong) || empty($latlong))

  {
   $response["address"] = "";
 }
 else
 {
   $latlongpoints= explode(',', $latlong ); 
   $address = getAddress($latlongpoints[0],$latlongpoints[1]); 
   $response["address"] = $address;
 }
 

            // fetching all user tasks
 $result = $db->getCartProducts($user_id);

 $response["error"] = false;
 $response["tasks"] = array();

            // looping through result and preparing tasks array
 while ($task = $result->fetch_assoc()) {
  $tmp = $task;
  array_push($response["tasks"], $tmp);
} 


echoRespnse(200, $response);
});



/**
 * fetching complet details of a project
 * method GET
 * url /getCompleteDetailsOfProject          
 */
$app->get('/getCompleteDetailsOfProject', function() use ($app) {

  verifyRequiredParams(array('project_id'));
  $project_id = $app->request->get('project_id');

  $response = array();
  $db = new DbHandler();

            // fetching all user tasks
  $result = $db->getCompleteDetailsOfProject($project_id);

  $response["error"] = false;
  $response["tasks"] = array();

            // looping through result and preparing tasks array
  while ($task = $result->fetch_assoc()) {
    $tmp = $task;
    array_push($response["tasks"], $tmp);
  } 
  echoRespnse(200, $response);
});


/**
 * fetching complet details of a project
 * method GET
 * url /getCompleteDetailsOfProject          
 */
$app->get('/getAvailableCareTakersofOrganisation', function() use ($app) {

  verifyRequiredParams(array('organistion_id'));
  $organistion_id = $app->request->get('organistion_id');

  $response = array();
  $db = new DbHandler();

            // fetching all user tasks
  $result = $db->getAvailableCareTakersofOrganisation($organistion_id);

  $response["error"] = false;
  $response["tasks"] = array();

            // looping through result and preparing tasks array
  while ($task = $result->fetch_assoc()) {
    $tmp = $task;
    array_push($response["tasks"], $tmp);
  } 
  echoRespnse(200, $response);
});

/**
 * fetching complet details of a project
 * method GET
 * url /getCompleteDetailsOfProject          
 */
$app->get('/getUserOrders', 'authenticate',function() use ($app) {


  global $user_id;


  $response = array();
  $db = new DbHandler();

            // fetching all user tasks
  $result = $db->getUserOrders($user_id);

  $response["error"] = false;
  $response["tasks"] = array();

            // looping through result and preparing tasks array
  while ($task = $result->fetch_assoc()) {
    $tmp = $task;
    if($tmp["status"] == 0 )

     $tmp['status'] = "Payment Pending";
   else   if($tmp["status"] == 1)
     $tmp['status'] = "Order Confirmed";

   if($tmp["payment_type"] == 0 )

     $tmp['payment_type'] = "Cash on delivery";
   else   if($tmp["payment_type"] == 1)
     $tmp['payment_type'] = "Online Payment";


   array_push($response["tasks"], $tmp);
 } 
 echoRespnse(200, $response);
});

/**
 * get user detilas by using mail
 * method GET
 * url /fetchUserDetailsbyMail          
 */
$app->get('/fetchUserDetailsbyMail', function() use ($app) {


 verifyRequiredParams(array('email'));


 $response = array();
 $db = new DbHandler();
 $email = $app->request->get('email');

 $user = $db->getUserByEmail($email);

 if ($user != NULL) {
  $response= $user;
  $response["error"] = false;

} else {
                    // unknown error occurred
  $response['error'] = true;
  $response['message'] = "An error occurred. Please try again";
}
echoRespnse(200, $response);
});


/**
 * get user detilas by using mail
 * method GET
 * url /fetchUserDetailsbyMail          
 */
$app->get('/fetchDoctorDetailsbyID', function() use ($app) {


 verifyRequiredParams(array('id'));


 $response = array();
 $db = new DbHandler();
 $id = $app->request->get('id');

 $user = $db->fetchDoctorDetailsbyID($id);

 if ($user != NULL) {
  $response= $user;
  $response["error"] = false;

} else {
                    // unknown error occurred
  $response['error'] = true;
  $response['message'] = "An error occurred. Please try again";
}
echoRespnse(200, $response);
});



/**
 * Listing all quotes of particual project
 * method GET
 * url /getQuotes          
 */
$app->get('/getQuotes', 'authenticate', function() use ($app) {


 verifyRequiredParams(array('project_id'));

 $project_id = $app->request->get('project_id');


 global $user_id;
 $response = array();
 $db = new DbHandler();

            // fetching all user tasks
 $result = $db->getProjectQuotes($project_id);

 $response["error"] = false;
 $response["tasks"] = array();
 
            // looping through result and preparing tasks array
 while ($task = $result->fetch_assoc()) {
  $tmp = $task;
  array_push($response["tasks"], $tmp);
} 
echoRespnse(200, $response);
});



/**
 * Listing all tasks of particual user
 * method GET
 * url /ongoingprojects          
 */
$app->get('/getProjectsUnderOrganisation', function() use ($app)  {

  verifyRequiredParams(array('organistion_id'));
  $organistion_id = $app->request->get('organistion_id');

  $response = array();
  $db = new DbHandler();

            // fetching all user tasks
  $result = $db->getProjectsUnderOrganisation($organistion_id);

  $response["error"] = false;
  $response["tasks"] = array();

            // looping through result and preparing tasks array
  while ($task = $result->fetch_assoc()) {
    $tmp = $task;
    array_push($response["tasks"], $tmp);
  } 
  echoRespnse(200, $response);
});


/**
 * Listing all tasks of particual user
 * method GET
 * url /ongoingprojects          
 */
$app->get('/getCareTakersOfAnOrganisation', function() use ($app)  {

  verifyRequiredParams(array('organistion_id'));
  $organistion_id = $app->request->get('organistion_id');

  $response = array();
  $db = new DbHandler();

            // fetching all user tasks
  $result = $db->getCareTakersOfAnOrganisation($organistion_id);

  $response["error"] = false;
  $response["tasks"] = array();

            // looping through result and preparing tasks array
  while ($task = $result->fetch_assoc()) {
    $tmp = $task;
    array_push($response["tasks"], $tmp);
  } 
  echoRespnse(200, $response);
});


/**
 * Fetching complete details of the caretaker
 * method GET
 * url /getCareTakerCompleteDetails          
 */
$app->get('/getCareTakerCompleteDetails', function() use ($app)  {

  verifyRequiredParams(array('id'));
  $id = $app->request->get('id');

  $response = array();
  $db = new DbHandler();

            // fetching all user tasks
  $result = $db->getCareTakerCompleteDetails($id);

  $response["error"] = false;
  $response["tasks"] = array();

            // looping through result and preparing tasks array
  while ($task = $result->fetch_assoc()) {
    $tmp = $task;
    array_push($response["tasks"], $tmp);
  } 
  echoRespnse(200, $response);
});


/**
 * Listing all tasks of particual user
 * method GET
 * url /ongoingprojects          
 */
$app->get('/getRentProducts', 'authenticate',function() use ($app)  {

  global $user_id;
  $response = array();
  $db = new DbHandler();
  $type= $app->request->get('type');



            // fetching all user tasks
  $result = $db->getRentProducts($user_id,$type);

  $response["error"] = false;
  $response["tasks"] = array();

            // looping through result and preparing tasks array
  while ($task = $result->fetch_assoc()) {
    $tmp = $task;
    array_push($response["tasks"], $tmp);
  } 
  echoRespnse(200, $response);
});


/**
 * Listing all tasks of particual user
 * method GET
 * url /ongoingprojects          
 */
$app->get('/getCompleteDetailofOrganisation',function() use ($app)  {

 verifyRequiredParams(array('organistion_id'));
 $organistion_id = $app->request->get('organistion_id');
 $response = array();
 $db = new DbHandler();

            // fetching all user tasks
 $result = $db->getCompleteDetailofOrganisation($organistion_id);

 $response["error"] = false;
 $response["tasks"] = array();

            // looping through result and preparing tasks array
 while ($task = $result->fetch_assoc()) {
  $tmp = $task;
  array_push($response["tasks"], $tmp);

} 
echoRespnse(200, $response);
});



/**
 * Listing all products
 * method GET
 * url /getRentProducts          
 */
$app->get('/getRentProducts', 'authenticate',function() use ($app)  {

  global $user_id;
  $response = array();
  $db = new DbHandler();

            // fetching all user tasks
  $result = $db->getRentProducts($user_id);

  $response["error"] = false;
  $response["tasks"] = array();

            // looping through result and preparing tasks array
  while ($task = $result->fetch_assoc()) {
    $tmp = $task;
    array_push($response["tasks"], $tmp);
  } 
  echoRespnse(200, $response);
});


/**
 * Listing all tasks of particual user
 * method GET
 * url /ongoingprojects          
 */
$app->get('/getProjectsOfOrganisation', function() use ($app)  {

  verifyRequiredParams(array('organistion_id'));
  $organistion_id = $app->request->get('organistion_id');

  $response = array();
  $db = new DbHandler();

            // fetching all user tasks
  $result = $db->getProjectsOfOrganisation($organistion_id);

  $response["error"] = false;
  $response["tasks"] = array();

            // looping through result and preparing tasks array
  while ($task = $result->fetch_assoc()) {
    $tmp = $task;
    array_push($response["tasks"], $tmp);
  } 
  echoRespnse(200, $response);
});



/**
 * Listing all tasks of particual user
 * method GET
 * url /ongoingprojects          
 */
$app->get('/getPendingProjectForOrganisation', function() use ($app)  {


 verifyRequiredParams(array('organistion_id'));
 $organistion_id = $app->request->get('organistion_id');
 $response = array();
 $db = new DbHandler();

            // fetching all user tasks
 $result = $db->getPendingProjectForOrganisation($organistion_id);

 $response["error"] = false;
 $response["tasks"] = array();

            // looping through result and preparing tasks array
 while ($task = $result->fetch_assoc()) {
  $tmp = $task;
  array_push($response["tasks"], $tmp);
} 
echoRespnse(200, $response);
});




/**
 * Listing single task of particual user
 * method GET
 * url /tasks/:id
 * Will return 404 if the task doesn't belongs to user
 */
$app->get('/tasks/:id', 'authenticate', function($task_id) {
  global $user_id;
  $response = array();
  $db = new DbHandler();

            // fetch task
  $result = $db->getTask($task_id, $user_id);

  if ($result != NULL) {
    $response["error"] = false;
    $response["id"] = $result["id"];
    $response["task"] = $result["task"];
    $response["status"] = $result["status"];
    $response["createdAt"] = $result["created_at"];
    echoRespnse(200, $response);
  } else {
    $response["error"] = true;
    $response["message"] = "The requested resource doesn't exists";
    echoRespnse(404, $response);
  }
});



function getAddress($latitude,$longitude){
  if(!empty($latitude) && !empty($longitude)){
        //Send request and receive json data by address
    $geocodeFromLatLong = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&sensor=false'); 
    $output = json_decode($geocodeFromLatLong);
    $status = $output->status;
        //Get address from json data
    $address = ($status=="OK")?$output->results[1]->formatted_address:'';
        //Return address of the given latitude and longitude
    if(!empty($address)){
      return $address;
    }else{
      return false;
    }
  }else{
    return false;   
  }
}   



/**
 * Creating new task in db
 * method POST
 * params - name
 * url - /tasks/
 */
$app->post('/createproject', 'authenticate', function() use ($app) {
            // check for required params
  verifyRequiredParams(array('gender','age','height','services','hours','duration','moreinfo','weight','latlong','name','address','user_name','email'));

  $response = array();
  $gender = $app->request->post('gender');
  $age = $app->request->post('age');
  $height = $app->request->post('height');
  $services = $app->request->post('services');
  $hours = $app->request->post('hours');
  $duration = $app->request->post('duration');
  $moreinfo = $app->request->post('moreinfo');
  $weight = $app->request->post('weight');
  $latlong = $app->request->post('latlong');
  $address = $app->request->post('address');
  $name= $app->request->post('name');
  $user_name= $app->request->post('user_name');
  $email= $app->request->post('email');
$address = $app->request->post('address');
  $latlongpoints= explode(',', $latlong ); 

 // $address = getAddress($latlongpoints[0],$latlongpoints[1]);

  global $user_id;
  $db = new DbHandler();

            // creating new task
  $task_id = $db->createProject($user_id, $name,$gender,$age,$height,$services,$hours,$duration,$moreinfo,$weight,$latlong,$address);

  if ($task_id != NULL) {
    $response["error"] = false;
    $response["message"] = "Task created successfully";
    $response["task_id"] = $task_id;
    SendingRFPmail($user_name,$email);
    sendPushNotificationtoTopics("GC","New Project required quotes");
    echoRespnse(200, $response);
  } else {
    $response["error"] = true;
    $response["message"] = "Failed to create task. Please try again";
    echoRespnse(200, $response);
  }            
});

/**
 * Updating existing task
 * method PUT
 * params task, status
 * url - /tasks/:id
 */
$app->post('/bookCareTaker', 'authenticate', function() use($app) {
            // check for required params
  verifyRequiredParams(array('project_id', 'caretaker_id','coated_price'));

  global $user_id;            
  $project_id = $app->request->put('project_id');
  $caretaker_id = $app->request->put('caretaker_id');
  $coated_price = $app->request->put('coated_price');

  $db = new DbHandler();
  $response = array();

            // updating task
  $result = $db->bookCareTaker($project_id, $caretaker_id, $coated_price);
  if ($result) {
                // task updated successfully

   $user = $db->fetchUserNameandFCMID($project_id,$caretaker_id);

   if ($user != NULL) {

     sendPushNotificationforOrganisation("GC",$user['first_name']." confirmed your caretaker",$user['fcm_id']);
     $response["error"] = false;
     $response["message"] = "Caretaker booked";   
   } else {
                    // unknown error occurred
    $response['error'] = true;
    $response['message'] = "An error occurred. Please try again";
  }




} else {
                // task failed to update
  $response["error"] = true;
  $response["message"] = "Task failed to update. Please try again!";
}
echoRespnse(200, $response);
});


/**
 * Updating existing task
 * method PUT
 * params task, status
 * url - /tasks/:id
 */
$app->post('/deleteItemFromCart', 'authenticate', function() use($app) {
            // check for required params
  verifyRequiredParams(array('cart_id'));

  global $user_id;            
  $cart_id= $app->request->post('cart_id');


  $db = new DbHandler();
  $response = array();

            // updating task
  $result = $db->deleteItemFromCart($cart_id, $user_id);
  if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "Task updated successfully";
  } else {
                // task failed to update
    $response["error"] = true;
    $response["message"] = "Task failed to update. Please try again!";
  }
  echoRespnse(200, $response);
});



/**
 * Updating project completion
 * method post
 * params project_id
 * url - /tasks/:id
 */
$app->post('/updateProjectCompletion', function() use($app) {
            // check for required params
  verifyRequiredParams(array('project_id'));


  $project_id = $app->request->put('project_id');


  $db = new DbHandler();
  $response = array();

            // updating task
  $result = $db->updateProjectCompletion($project_id);
  if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "Task updated successfully";
  } else {
                // task failed to update
    $response["error"] = true;
    $response["message"] = "Task failed to update. Please try again!";
  }
  echoRespnse(200, $response);
});


/**
 * update user details
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/updateUserDetails', 'authenticate', function() use($app) {
            // check for required params
  verifyRequiredParams(array('first_name', 'last_name','email','mobile','first_lanugage'));

  global $user_id;            
  $first_name = $app->request->put('first_name');
  $last_name = $app->request->put('last_name');
  $email = $app->request->put('email');
  $mobile = $app->request->put('mobile');
  $first_lanugage= $app->request->put('first_lanugage');
  $second_language= $app->request->put('second_language');
  $third_language= $app->request->put('third_language');

  if(is_null($second_language))

    $second_language= "";

  if(is_null($third_language))

    $third_language= "";


  $db = new DbHandler();
  $response = array();

            // updating task
  $result = $db->updateUserDetails($user_id, $first_name, $last_name,$email,$mobile,$first_lanugage,$second_language,$third_language);
  if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "Task updated successfully";
  } else {
                // task failed to update
    $response["error"] = true;
    $response["message"] = "Task failed to update. Please try again!";
  }
  echoRespnse(200, $response);
});



/**
 * update doctor details
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/updateDoctorDetails', function() use($app) {
            // check for required params
 verifyRequiredParams(array( 'email','mobile','name','age','gender','location','latlong','specialiation','experience','time_slots','id'));

  $response = array();

            // reading post params

  $email = $app->request->post('email');
  $mobile = $app->request->post('mobile');
  $name= $app->request->post('name');
  $last_name = $app->request->post('last_name');
  
  $age= $app->request->post('age');
  $gender= $app->request->post('gender');
  $location= $app->request->post('location');
  $latlong= $app->request->post('latlong');
  $specialiation= $app->request->post('specialiation');
  $experience= $app->request->post('experience');
  $additional_degree= $app->request->post('additional_degree');
  $time_slots= $app->request->post('time_slots');
   $id= $app->request->post('id');

  if(is_null($email ))

    $email = "";


  if(is_null($mobile ))

    $mobile = "";

  if(is_null($additional_degree))

    $additional_degree= "";



            // validating email address
  validateEmail($email);


  $db = new DbHandler();
  $response = array();

            // updating task
  $result = $db->updateDoctorDetails($name,$age,$email,$mobile,$gender,$location,$latlong,$specialiation,$experience,$additional_degree,$time_slots,$id);
  if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "Task updated successfully";
  } else {
                // task failed to update
    $response["error"] = true;
    $response["message"] = "Task failed to update. Please try again!";
  }
  echoRespnse(200, $response);
});


/**
 * update user details
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/updateUserFCMID', 'authenticate', function() use($app) {
            // check for required params
  verifyRequiredParams(array('fcm_id'));

  global $user_id;            
  $fcm_id = $app->request->put('fcm_id');


  $db = new DbHandler();
  $response = array();

            // updating task
  $result = $db->updateUserFCMID($user_id, $fcm_id);
  if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "Task updated successfully";
  } else {
                // task failed to update
    $response["error"] = true;
    $response["message"] = "Task failed to update. Please try again!";
  }
  echoRespnse(200, $response);
});


/**
 * update organisation fcm id
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/updateOrganisationFcmID', function() use($app) {
            // check for required params
  verifyRequiredParams(array('organisation_id','fcm_id'));

  global $user_id;            
  $fcm_id = $app->request->put('fcm_id');
  $organisation_id = $app->request->put('organisation_id');

  $db = new DbHandler();
  $response = array();

            // updating task
  $result = $db->updateOrganisationFcmID($organisation_id, $fcm_id);
  if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "Task updated successfully";
  } else {
                // task failed to update
    $response["error"] = true;
    $response["message"] = "Task failed to update. Please try again!";
  }
  echoRespnse(200, $response);
});


/**
 * update organisation fcm id
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/updateDoctorFcmID', function() use($app) {
            // check for required params
  verifyRequiredParams(array('doctor_id','fcm_id'));

  global $user_id;            
  $fcm_id = $app->request->put('fcm_id');
  $doctor_id = $app->request->put('doctor_id');

  $db = new DbHandler();
  $response = array();

            // updating task
  $result = $db->updateDoctorFcmID($doctor_id, $fcm_id);
  if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "Task updated successfully";
  } else {
                // task failed to update
    $response["error"] = true;
    $response["message"] = "Task failed to update. Please try again!";
  }
  echoRespnse(200, $response);
});



/**
 * update user details
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/updateUserProfilePicture', 'authenticate', function() use($app) {
            // check for required params


  global $user_id;            


  $db = new DbHandler();
  $response = array();

  $file_path = "images/";

  $file_path = $file_path . basename( $_FILES['uploaded_file']['name']);

  if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $file_path)) {

   $result = $db->updateProfilePicPathByUserId($file_path, $user_id);
   if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "Profile Pic updated successfully";
  } else {
                // task failed to update
   $response["error"] = true;
   $response["message"] = "Updation failed";
 }


} else{

  $response["error"] = true;
  $response["message"] = "Updation failed";

}

echoRespnse(200, $response);
});


/**
 * update user details
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/updateDoctorProfilePicture', function() use($app) {
            // check for required params


  verifyRequiredParams(array('id'));

          
  $id = $app->request->put('id');
       


  $db = new DbHandler();
  $response = array();

  $file_path = "images/";

  $file_path = $file_path . basename( $_FILES['uploaded_file']['name']);

  if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $file_path)) {

   $result = $db->updateDoctorProfilePicPathByUserId($file_path, $id);
   if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "Profile Pic updated successfully";
  } else {
                // task failed to update
   $response["error"] = true;
   $response["message"] = "Updation failed";
 }


} else{

  $response["error"] = true;
  $response["message"] = "Updation failed";

}

echoRespnse(200, $response);
});


/**
 * update care taker profile picture
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/updateCareTakerProfilePicture', function() use($app) {
            // check for required params



 verifyRequiredParams(array( 'id'));
 $id = $app->request->post('id');

 $db = new DbHandler();
 $response = array();

 $file_path = "images/";

 $file_path = $file_path . basename( $_FILES['uploaded_file']['name']);

 if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $file_path)) {

   $result = $db->updateCareTakerProfilePicture($file_path, $id);
   if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "Profile Pic updated successfully";
  } else {
                // task failed to update
   $response["error"] = true;
   $response["message"] = "Updation failed";
 }


} else{

  $response["error"] = true;
  $response["message"] = "Updations failed";

}

echoRespnse(200, $response);
});


/**
 * update care taker profile picture
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/updateAadharCard', function() use($app) {
            // check for required params



 verifyRequiredParams(array( 'id'));
 $id = $app->request->post('id');

 $db = new DbHandler();
 $response = array();

 $file_path = "images/";

 $file_path = $file_path . basename( $_FILES['uploaded_file']['name']);

 if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $file_path)) {

   $result = $db->updateAadharCard($file_path, $id);
   if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "Image updated successfully";
  } else {
                // task failed to update
   $response["error"] = true;
   $response["message"] = "Updation failed";
 }


} else{

  $response["error"] = true;
  $response["message"] = "Updations failed";

}

echoRespnse(200, $response);
});

/**
 * update care taker profile picture
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/updatePoliceCertificate', function() use($app) {
            // check for required params



 verifyRequiredParams(array( 'id'));
 $id = $app->request->post('id');

 $db = new DbHandler();
 $response = array();

 $file_path = "images/";

 $file_path = $file_path . basename( $_FILES['uploaded_file']['name']);

 if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $file_path)) {

   $result = $db->updatePoliceCertifcateOfCaretaker($file_path, $id);
   if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "Certificate updated successfully";
  } else {
                // task failed to update
   $response["error"] = true;
   $response["message"] = "Updation failed";
 }


} else{

  $response["error"] = true;
  $response["message"] = "Updations failed";

}

echoRespnse(200, $response);
});

/**
 * update care taker profile picture
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/updateExtraCertificate', function() use($app) {
            // check for required params



 verifyRequiredParams(array( 'id'));
 $id = $app->request->post('id');

 $db = new DbHandler();
 $response = array();

 $file_path = "images/";

 $file_path = $file_path . basename( $_FILES['uploaded_file']['name']);

 if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $file_path)) {

   $result = $db->updateExtracertificate($file_path, $id);
   if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "Certificate updated successfully";
  } else {
                // task failed to update
   $response["error"] = true;
   $response["message"] = "Updation failed";
 }


} else{

  $response["error"] = true;
  $response["message"] = "Updations failed";

}

echoRespnse(200, $response);
});

/**
 * update user details
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/updateCareTakerDetails', function() use($app) {
            // check for required params
 verifyRequiredParams(array( 'gender','name','id','age','experience','designation','addressproof','blood_group','martial_status','address'));



 $email = $app->request->post('email');
 $mobile = $app->request->post('mobile');
 $name = $app->request->post('name');
 $gender = $app->request->post('gender');
 $id = $app->request->post('id');
 $age = $app->request->post('age');
 $doj = $app->request->post('doj');
 $qualification = $app->request->post('qualification');
 $experience = $app->request->post('experience');
 $designation = $app->request->post('designation');
 $addressproof = $app->request->post('addressproof');
 $blood_group = $app->request->post('blood_group');
 $martial_status = $app->request->post('martial_status');
 $residence_number = $app->request->post('residence_number');
 $father = $app->request->post('father');
 $mother = $app->request->post('mother');
 $spouse = $app->request->post('spouse');
 $children = $app->request->post('children');
 $background_verification = $app->request->post('background_verification');
 $address = $app->request->post('address');

 if(is_null($residence_number))

    $residence_number = "";

  if(is_null($father))
    $father="";
  if(is_null($mother))
    $mother = "";
  if(is_null($spouse))
    $spouse="";
  if(is_null($children))
    $children="";

  if(is_null($email))
    $email="";

  if(is_null($mobile))
    $mobile="";

  if(is_null($doj))
    $doj="";

  if(is_null($qualification))
    $qualification="";

  if(is_null($background_verification))
    $background_verification="";

            // validating email address


$db = new DbHandler();
$result = $db->updateCareTakerDetails($id, $name,$email,$mobile, $gender,$age,$doj,$qualification,$experience,$designation,$addressproof,$blood_group,$martial_status,$residence_number,$father,$mother,$spouse,$children,$background_verification,$address);



if ($result) {
                // task updated successfully
  $response["error"] = false;
  $response["message"] = "Task updated successfully";
} else {
                // task failed to update
  $response["error"] = true;
  $response["message"] = "Task failed to update. Please try again!";
}
echoRespnse(200, $response);
});

/**
 * add item to cart
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/addItemToCart','authenticate', function() use($app) {
            // check for required params
  verifyRequiredParams(array('product_id'));

  global $user_id;        
  $product_id = $app->request->put('product_id');


  $db = new DbHandler();
  $response = array();

            // updating task
  $result = $db->addItemToCart($user_id,$product_id);
  if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "Item added successfully";
  } else {
                // task failed to update
    $response["error"] = true;
    $response["message"] = "Product may be out of stock or something went wrong";
  }
  echoRespnse(200, $response);
});


/**
 * confirming quote made by an organisation
 * method POST
 * params project_id, caretaker_id,coated_price
 * url - /confimQuote
 */
$app->post('/confimQuote', function() use($app) {
            // check for required params
  verifyRequiredParams(array('project_id','caretaker_id','coated_price'));


  $project_id = $app->request->put('project_id');
  $caretaker_id = $app->request->put('caretaker_id');
  $coated_price = $app->request->put('coated_price');


  $db = new DbHandler();
  $response = array();

            // updating task
  $result = $db->projectQuotebyOrganisation($caretaker_id,$project_id,$coated_price);
  if ($result) {
                // task updated successfully


    $user = $db->fetchOrganisationNameandFCMID($project_id,$caretaker_id);

    if ($user != NULL) {

     sendPushNotifications("GC",$user['organisations_name']." proposed a quote for your request",$user['fcm_id']);
     $response["error"] = false;
     $response["message"] = "project quoted";   
   } else {
                    // unknown error occurred
    $response['error'] = true;
    $response['message'] = "An error occurred. Please try again";
  }



} else {
                // task failed to update
  $response["error"] = true;
  $response["message"] = "Care taker is not available";
}
echoRespnse(200, $response);
});


/**
 * remove item from cart
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/updateCartCountbyRemovingOneItem','authenticate', function() use($app) {
            // check for required params
  verifyRequiredParams(array('product_id'));

  global $user_id;        
  $product_id = $app->request->put('product_id');


  $db = new DbHandler();
  $response = array();

            // updating task
  $result = $db->updateCartCountbyRemovingOneItem($user_id,$product_id);
  if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "Item removed successfully";
  } else {
                // task failed to update
    $response["error"] = true;
    $response["message"] = "Something went wrong";
  }
  echoRespnse(200, $response);
});



/**
 * update quote details
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/updateQuoteDetails', function() use($app) {
            // check for required params
  verifyRequiredParams(array('caretaker_id','coated_price','quote_id','oldcareTakerId'));


  $quote_id= $app->request->put('quote_id');
  $coated_price= $app->request->put('coated_price');
  $caretaker_id= $app->request->put('caretaker_id');
  $oldcareTakerId= $app->request->put('oldcareTakerId');


  $db = new DbHandler();
  $response = array();

            // updating task
  $result = $db->updateQuote($quote_id,$coated_price,$caretaker_id,$oldcareTakerId);
  if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "updated quote details";
  } else {
                // task failed to update
    $response["error"] = true;
    $response["message"] = "Something went wrong";
  }
  echoRespnse(200, $response);
});



/**
 * update quote details
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/deleteCareTaker', function() use($app) {
            // check for required params
  verifyRequiredParams(array('caretaker_id'));



  $caretaker_id= $app->request->put('caretaker_id');



  $db = new DbHandler();
  $response = array();

            // updating task
  $result = $db->deleteCareTaker($caretaker_id);
  if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "Caretaker removed";
  } else {
                // task failed to update
    $response["error"] = true;
    $response["message"] = "Something went wrong";
  }
  echoRespnse(200, $response);
});


/**
 * cancel quote
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/cancelQuote', function() use($app) {
            // check for required params
  verifyRequiredParams(array('caretaker_id','quote_id'));


  $quote_id= $app->request->put('quote_id');

  $caretaker_id= $app->request->put('caretaker_id');


  $db = new DbHandler();
  $response = array();

            // updating task
  $result = $db->cancelQuote($quote_id,$caretaker_id);
  if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "updated quote details";
  } else {
                // task failed to update
    $response["error"] = true;
    $response["message"] = "Something went wrong";
  }
  echoRespnse(200, $response);
});



/**
 * Deleting task. Users can delete only their tasks
 * method DELETE
 * url /tasks
 */
$app->delete('/tasks/:id', 'authenticate', function($task_id) use($app) {
  global $user_id;

  $db = new DbHandler();
  $response = array();
  $result = $db->deleteTask($user_id, $task_id);
  if ($result) {
                // task deleted successfully
    $response["error"] = false;
    $response["message"] = "Task deleted succesfully";
  } else {
                // task failed to delete
    $response["error"] = true;
    $response["message"] = "Task failed to delete. Please try again!";
  }
  echoRespnse(200, $response);
});



/*-------------website apis--------------------*/


/**
 * cancel quote
 * method POST
 * params task, status
 * url - /tasks/:id
 */
$app->post('/bookDoctorAppointmentFromWebsite', function() use($app) {
            // check for required params
  verifyRequiredParams(array('doctor_id','user_id','time_slot','appointment_date'));


  $doctor_id= $app->request->post('doctor_id');

  $user_id= $app->request->post('user_id');
$time_slot= $app->request->post('time_slot');
$appointment_date= $app->request->post('appointment_date');
$latlong = "12.959366, 77.707893";
$address="Purva Riviera,Old Airport Road, Marathahalli, Lakshminarayana Pura, Munnekollal, Bengaluru, Karnataka 560037";
$created_at = date('d-m-Y H:i a');
  $db = new DbHandler();
  $response = array();

   $result = $db->fetchAllSlotsOfDoctors($doctor_id);
    $doctor_slots;
    foreach($result as $doctor_slot)
    {
      $doctor_slots = explode(',', $doctor_slot);  
    }
   
    $actual_time_slot = 0;
    

     foreach ($doctor_slots as $doctor_slot) {
      
      $temp_slot = substr($doctor_slot, 0,1);
      $temp_booked_slot = substr($time_slot, 0,1);

//echo($temp_booked_slot." ".$temp_slot." ". ($temp_booked_slot-1));
      if($temp_booked_slot == $temp_slot || ($temp_booked_slot-1) == $temp_slot)
      {
         $actual_time_slot = $doctor_slot;
      }


     }

//echo($actual_time_slot);
            // updating task
  $result = $db->bookDoctorAppointmentFromWebsite($doctor_id,$user_id,$actual_time_slot,$time_slot,$appointment_date,$created_at,$latlong,$address);
  if ($result!=NULL) {
                // task updated successfully

$fcm_id = $db->fetchFcmIDOfDoctors($doctor_id);

   if($fcm_id!=null)
   {

     //sendPushNotificationforOrganisation("GC","You got an appointment",$fcm_id['fcm_id']);
      $response["error"] = false;
    $response["message"] = "updated quote details";
   }
   else
   {
     $response["error"] = false;
    $response["message"] = "updated quote details";
   }


   
  } else {
                // task failed to update
    $response["error"] = true;
    $response["message"] = "Something went wrong";
  }
  echoRespnse(200, $response);
});


/**
 * Updating existing task
 * method PUT
 * params task, status
 * url - /tasks/:id
 */
$app->post('/cancelDoctorAppointment', function() use($app) {
            // check for required params
  verifyRequiredParams(array('doctor_id','appointment_id'));

  global $user_id;            
  $doctor_id= $app->request->post('doctor_id');
  $appointment_id= $app->request->post('appointment_id');


  $db = new DbHandler();
  $response = array();

            // updating task
  $result = $db->cancelDoctorAppointment($doctor_id, $appointment_id);
  if ($result) {
                // task updated successfully
    $response["error"] = false;
    $response["message"] = "Task updated successfully";
  } else {
                // task failed to update
    $response["error"] = true;
    $response["message"] = "Task failed to update. Please try again!";
  }
  echoRespnse(200, $response);
});


/**
 * Listing all tasks of particual user
 * method GET
 * url /ongoingprojects          
 */
$app->get('/fetchDotorPastAppointments', function() use($app) {


verifyRequiredParams(array('doctor_id'));
$appointment_date = date('j/n/Y');
 $doctor_id= $app->request->get('doctor_id');
  global $user_id;
  $response = array();
  $db = new DbHandler();

            // fetching all user tasks
  $result = $db->fetchDotorPastAppointments($doctor_id,$appointment_date);

  $response["error"] = false;
  $response["tasks"] = array();

            // looping through result and preparing tasks array
  while ($task = $result->fetch_assoc()) {
    $tmp = $task;
    array_push($response["tasks"], $tmp);
  } 


  echoRespnse(200, $response);
});



/**
 * Listing all tasks of particual user
 * method GET
 * url /ongoingprojects          
 */
$app->get('/fetchDotorAppointments', function() use($app) {


verifyRequiredParams(array('doctor_id'));
$appointment_date = date('j/n/Y');
 $doctor_id= $app->request->get('doctor_id');
  global $user_id;
  $response = array();
  $db = new DbHandler();

            // fetching all user tasks
  $result = $db->fetchDotorAppointments($doctor_id,$appointment_date);

  $response["error"] = false;
  $response["tasks"] = array();

            // looping through result and preparing tasks array
  while ($task = $result->fetch_assoc()) {
    $tmp = $task;
    array_push($response["tasks"], $tmp);
  } 


  echoRespnse(200, $response);
});


/**
 * Listing all tasks of particual user
 * method GET
 * url /ongoingprojects          
 */
$app->get('/fetchAvailableDoctors', 'authenticate', function() use($app) {


verifyRequiredParams(array('appointment_date'));
$appointment_date= $app->request->get('appointment_date');
  global $user_id;
  $response = array();
  $db = new DbHandler();

            // fetching all user tasks
  $result = $db->fetchAvailableDoctors($appointment_date,$user_id);

  $response["error"] = false;
  $response["tasks"] = array();

            // looping through result and preparing tasks array
  while ($task = $result->fetch_assoc()) {
    $tmp = $task;
    array_push($response["tasks"], $tmp);
  } 


  echoRespnse(200, $response);
});


/**
 * Listing all tasks of particual user
 * method GET
 * url /ongoingprojects          
 */
$app->get('/fetchAvailableSlots', 'authenticate', function() use($app) {


verifyRequiredParams(array('appointment_date','doctor_id'));
$appointment_date= $app->request->get('appointment_date');
$doctor_id= $app->request->get('doctor_id');
  global $user_id;
  $response = array();
  $db = new DbHandler();

            // fetching all user tasks
  $result = $db->fetchAvailableSlots($appointment_date,$doctor_id);

   $bookedSlots=[];
   while ($task = $result->fetch_assoc()) {
    $bookedSlots[] = $task['timeslot_sub'];
    
  } 

  if(sizeof($bookedSlots)>0)
  {

//echo($bookedSlots[0]);
  $doctor_slots = $db->fetchAllSlotsOfDoctors($doctor_id);
    
    
    foreach ($doctor_slots as $doctor_slot) {
       
        $slotsArray = explode(',', $doctor_slot);
    $final_slots = []; 
    foreach ($slotsArray as $slot) {
               

             $slot_temp =  explode('-', $slot);

             //echo($slot_temp[0]." ".filter_var($slot_temp[0], FILTER_SANITIZE_NUMBER_INT));
             $start_time = filter_var($slot_temp[0], FILTER_SANITIZE_NUMBER_INT);
             
             $final_slots[] = $start_time.":00 to ".$start_time.":15";
             $final_slots[] = $start_time.":15 to ".$start_time.":30";
             $final_slots[] = $start_time.":30 to ".$start_time.":45";
             $final_slots[] = $start_time.":45 to ".($start_time+1).":00";

             $final_slots[] =($start_time+1).":00 to ".($start_time+1).":15";
             $final_slots[] = ($start_time+1).":15 to ".($start_time+1).":30";
             $final_slots[] = ($start_time+1).":30 to ".($start_time+1).":45";
             $final_slots[] = ($start_time+1).":45 to ".($start_time+2).":00";





   }
    
    $key = array_search($bookedSlots[0], $final_slots);


         $upper_slots_count =0;
          $final_available_slots = [];
         for($j=$key;$j>=0;$j--)
         {
            if(!in_array($final_slots[$j], $bookedSlots))
            {

              $upper_slots_count = $upper_slots_count+1;

              if($upper_slots_count<3)
              {
                //echo("upper: ".$final_slots[$j]);
                $final_available_slots[] = $final_slots[$j]; //adding only two avaible upper bound slots
              }
              

            }

         }


         $lower_slots_count = 0;

         for ($k=$key+1;$k<sizeof($final_slots);$k++)
         {
          if(!in_array($final_slots[$k], $bookedSlots))
            {

              $lower_slots_count = $lower_slots_count+1;

              if($lower_slots_count<3)
              {
                // echo("lower: ".$final_slots[$k]);
                $final_available_slots[] = $final_slots[$k]; //adding only two avaible lower bound slots
              }
              

            }
         }


        

    
        $response["error"] = false;
  $response["tasks"] = array();
array_push($response["tasks"], $final_available_slots);
        /*    // looping through result and preparing tasks array
  while ($task = $result->fetch_assoc()) {
    $tmp = $task;
    array_push($response["tasks"], $tmp);
  } */


  echoRespnse(200, $response);
    
 


    }



}
else
{
    $doctor_slots = $db->fetchAllSlotsOfDoctors($doctor_id);
    
    
    foreach ($doctor_slots as $doctor_slot) {
       
        $slotsArray = explode(',', $doctor_slot);
    $final_slots = []; 
    foreach ($slotsArray as $slot) {
               

             $slot_temp =  explode('-', $slot);

             //echo($slot_temp[0]." ".filter_var($slot_temp[0], FILTER_SANITIZE_NUMBER_INT));
             $start_time = filter_var($slot_temp[0], FILTER_SANITIZE_NUMBER_INT);
             
            $final_slots[] = $start_time.":00 to ".$start_time.":15";
             $final_slots[] = $start_time.":15 to ".$start_time.":30";
             $final_slots[] = $start_time.":30 to ".$start_time.":45";
             $final_slots[] = $start_time.":45 to ".($start_time+1).":00";

 $final_slots[] =($start_time+1).":00 to ".($start_time+1).":15";
             $final_slots[] = ($start_time+1).":15 to ".($start_time+1).":30";
             $final_slots[] = ($start_time+1).":30 to ".($start_time+1).":45";
             $final_slots[] = ($start_time+1).":45 to ".($start_time+2).":00";




   }
    }
   

    $response["error"] = false;
  $response["tasks"] = array();
array_push($response["tasks"], $final_slots);
        /*    // looping through result and preparing tasks array
  while ($task = $result->fetch_assoc()) {
    $tmp = $task;
    array_push($response["tasks"], $tmp);
  } */


  echoRespnse(200, $response); 

  }
   




});





/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
  $error = false;
  $error_fields = "";
  $request_params = array();
  $request_params = $_REQUEST;
    // Handling PUT request params
  if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
    $app = \Slim\Slim::getInstance();
    parse_str($app->request()->getBody(), $request_params);
  }
  foreach ($required_fields as $field) {
    if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
      $error = true;
      $error_fields .= $field . ', ';
    }
  }

  if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
    $response = array();
    $app = \Slim\Slim::getInstance();
    $response["error"] = true;
    $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
    echoRespnse(400, $response);
    $app->stop();
  }
}

/**
 * Validating email address
 */
function validateEmail($email) {
  $app = \Slim\Slim::getInstance();
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $response["error"] = true;
    $response["message"] = 'Email address is not valid';
    echoRespnse(400, $response);
    $app->stop();
  }
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
  $app = \Slim\Slim::getInstance();
    // Http response code
  $app->status($status_code);

    // setting response content type to json
  $app->contentType('application/json');

  echo json_encode($response);
}


function sendPushNotifications($title,$message,$fcm_id)
{
 $firebase = new Firebase();
 $push = new Push();
 $push->setTitle($title);
 $push->setMessage($message);
 $push->setImage('');
 $push->setIsBackground(FALSE);
 $payload = array();
 $payload['title'] = $title;

 $payload['message'] = $message;
 $push->setPayload($payload);

 $json = $push->getPush();

 $responses = $firebase->send($fcm_id, $json,0);


}

function sendPushNotificationforOrganisation($title,$message,$fcm_id)
{
 $firebase = new Firebase();
 $push = new Push();
 $push->setTitle($title);
 $push->setMessage($message);
 $push->setImage('');
 $push->setIsBackground(FALSE);
 $payload = array();
 $payload['title'] = $title;

 $payload['message'] = $message;
 $push->setPayload($payload);
 $json = $push->getPush();

 $responses = $firebase->send($fcm_id, $json,1);

 
}

function sendMultiplePushNotificationforOrganisation($title,$message,$fcm_id)
{
 $firebase = new Firebase();
 $push = new Push();
 $push->setTitle($title);
 $push->setMessage($message);
 $push->setImage('');
 $push->setIsBackground(FALSE);
 $payload = array();
 $payload['title'] = $title;

 $payload['message'] = $message;
 $push->setPayload($payload);
 $json = $push->getPush();

 $responses = $firebase->send($fcm_id, $json,1);
}


function sendPushNotificationtoTopics($title,$message)
{
 $firebase = new Firebase();
 $push = new Push();
 $push->setTitle($title);
 $push->setMessage($message);
 $push->setImage('');
 $push->setIsBackground(FALSE);
 $payload = array();
 $payload['title'] = $title;

 $payload['message'] = $message;
 $push->setPayload($payload);
 $json = $push->getPush();
 $responses = $firebase-> sendToTopic('organisations', $json,1);

}


function PercentageDiscountCalculator($percentage,$totalprice)
{



  return ($percentage * $totalprice)/100;
}

function sendRegisterMailtoUser($user_name,$email)
{

  $mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

/*$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'user@example.com';                 // SMTP username
$mail->Password = 'secret';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;    */                              // TCP port to connect to

$mail->setFrom('from@example.com', 'DoDo');
$mail->addAddress($email,$user_name);     // Add a recipient

$mail->addReplyTo('info@example.com', 'DoDo');
//$mail->addCC('cc@example.com');
//$mail->addBCC('bcc@example.com');

/*$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
$mail->addAttachment('/tmp/image.jpg', 'new.jpg');*/    // Optional name
$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Welcome to DoDo!';
$mail->Body    = '<p>Dear '.$user_name.'</p>,
<p>Thank you for playing your part in the revival of Doctors2Doors. We appreciate your effort and hope we have solved your pressing healthcare issue.</p></br> 

<p>If you thought we did a good job, help another in need and be their DoDo. Use your personal referral code given below to gift a discount of 10% and avail one on your next use of our services. </p></br>

<p>We look forward to a long and fruitful partnership with you and once again, from DoDo’s everywhere, thank you.</p></br>

<p>Best,</p>

<p>Team DoDo</p>
';

if(!$mail->send()) {
   /* echo 'Message could not be sent.';
   echo 'Mailer Error: ' . $mail->ErrorInfo;*/

   $txt = $mail->ErrorInfo;
   $myfile = file_put_contents('request.log', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);

 } else {
  /*echo 'Message has been sent';*/
  $txt = "Message has been sent".$user_name.$email;
  $myfile = file_put_contents('request.log', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);

}


}



function SendingRFPmail($user_name,$email)
{

  $mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

/*$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'user@example.com';                 // SMTP username
$mail->Password = 'secret';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;    */                              // TCP port to connect to

$mail->setFrom('from@example.com', 'DoDo');
$mail->addAddress($email,$user_name);     // Add a recipient

$mail->addReplyTo('info@example.com', 'DoDo');
//$mail->addCC('cc@example.com');
//$mail->addBCC('bcc@example.com');

/*$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
$mail->addAttachment('/tmp/image.jpg', 'new.jpg');*/    // Optional name
$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = "DoDo's Activated";
$mail->Body    = '<p>Dear '.$user_name.',</p>
<p>Your DoDo’s are on their way. They’ll be back soon with offers from the different service providers. 
</p></br> 

<p>Stay glued to your phone for the notifications. Please note, these quotes are valid for only 10 hours after which, the quote is subject to change based on the availability of the CarePro.</p></br>

<p>Please feel free to contact us at any point if you have any questions. We’re always there to help you. </p></br>

<p>Thanks and Best,
</p>

<p>Team DoDo</p>
';

if(!$mail->send()) {
   /* echo 'Message could not be sent.';
   echo 'Mailer Error: ' . $mail->ErrorInfo;*/

   $txt = $mail->ErrorInfo;
   $myfile = file_put_contents('request.log', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);

 } else {
    // echo 'Message has been sent'.$user_name.$email; 
  $txt = "Message has been sent".$user_name.$email;
  $myfile = file_put_contents('request.log', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);
}


}



$app->run();
?>